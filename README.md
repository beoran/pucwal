# pucwal

Pure C watch library. A library for the T-WATCH-2020, using esp-idf.
Intended as an alternative to the official Arduino based, for people 
like me, who prefer cleaner, pure C.
