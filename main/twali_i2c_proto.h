
int twali_i2c_master_init(twali_i2c_port_t port, twali_gpio_pin_t sda, twali_gpio_pin_t sclk, twali_i2c_speed_t speed);
int twali_i2c_get_bytes(twali_i2c_port_t i2c_port, twali_i2c_address_t chip_addr, twali_i2c_register_t data_addr, int len, uint8_t * data); 
int twali_i2c_set_bytes(twali_i2c_port_t i2c_port, twali_i2c_address_t chip_addr, twali_i2c_register_t data_addr, int len, uint8_t * data);
int twali_i2c_detect(twali_i2c_port_t i2c_port, uint8_t found[128], int8_t debug);
