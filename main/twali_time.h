#ifndef TWALI_TIME_H_INCLUDED
#define TWALI_TIME_H_INCLUDED

#include <stdint.h>
#include "twali_time_proto.h"

#ifdef CPROTO
#undef IRAM_ATTR
#define IRAM_ATTR
#else 
#include "esp_system.h"
#include "esp_timer.h"
#endif

#define NOP() asm volatile ("nop")

static inline uint32_t IRAM_ATTR twali_time_us() {
    return (esp_timer_get_time());
}

static inline void IRAM_ATTR twali_time_delay_us(uint32_t us) {
    uint32_t m = twali_time_us();
    if(us){
        uint32_t e = (m + us);
        if(m > e){ //overflow
            while(twali_time_us() > e){
                NOP();
            }
        }
        while(twali_time_us() < e){
            NOP();
        }
    }
}



#endif
