#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_vfs.h"
#include "esp_spiffs.h"
#include "driver/i2c.h"

#include "st7789.h"
#include "fontx.h"
#include "bmpfile.h"
#include "decode_image.h"
#include "pngle.h"
#include "axp20x.h"
#include "bma4_defs.h"
#include "bma4.h"
#include "bma423.h"
#include "twali_i2c.h"
#include "twali_i2s.h"
#include "twali_bma.h"
#include "twali_time.h"
#include "ft6236.h"
#include "pcf8563.h"

#define	INTERVAL		400
#define WAIT	vTaskDelay(INTERVAL)



static const char *TAG = "ST7789";

static twali_temperature_celcius watch_temperature = 0;
twali_step_count 				 watch_step_count = 0;
static int touch_x = 0;
static int touch_y = 0;

static void SPIFFS_Directory(char * path) {
	DIR* dir = opendir(path);
	assert(dir != NULL);
	while (true) {
		struct dirent*pe = readdir(dir);
		if (!pe) break;
		ESP_LOGI(__FUNCTION__,"d_name=%s d_ino=%d d_type=%x", pe->d_name,pe->d_ino, pe->d_type);
	}
	closedir(dir);
}

// You have to set these CONFIG value using menuconfig.
#if 0
#define CONFIG_WIDTH  240
#define CONFIG_HEIGHT 240
#define CONFIG_MOSI_GPIO 23
#define CONFIG_SCLK_GPIO 18
#define CONFIG_CS_GPIO -1
#define CONFIG_DC_GPIO 19
#define CONFIG_RESET_GPIO 15
#define CONFIG_BL_GPIO -1
#endif

TickType_t FillTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	lcdFillScreen(dev, RED);
	vTaskDelay(50);
	lcdFillScreen(dev, GREEN);
	vTaskDelay(50);
	lcdFillScreen(dev, BLUE);
	vTaskDelay(50);

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t ColorBarTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	if (width < height) {
		uint16_t y1,y2;
		y1 = height/3;
		y2 = (height/3)*2;
		lcdDrawFillRect(dev, 0, 0, width-1, y1-1, RED);
		vTaskDelay(1);
		lcdDrawFillRect(dev, 0, y1, width-1, y2-1, GREEN);
		vTaskDelay(1);
		lcdDrawFillRect(dev, 0, y2, width-1, height-1, BLUE);
	} else {
		uint16_t x1,x2;
		x1 = width/3;
		x2 = (width/3)*2;
		lcdDrawFillRect(dev, 0, 0, x1-1, height-1, RED);
		vTaskDelay(1);
		lcdDrawFillRect(dev, x1, 0, x2-1, height-1, GREEN);
		vTaskDelay(1);
		lcdDrawFillRect(dev, x2, 0, width-1, height-1, BLUE);
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t ArrowTest(TFT_t * dev, FontxFile *fx, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	// get font width & height
	uint8_t buffer[FontxGlyphBufSize];
	uint8_t fontWidth;
	uint8_t fontHeight;
	GetFontx(fx, 0, buffer, &fontWidth, &fontHeight);
	//ESP_LOGI(__FUNCTION__,"fontWidth=%d fontHeight=%d",fontWidth,fontHeight);
	
	uint16_t xpos;
	uint16_t ypos;
	int	stlen;
	uint8_t ascii[24];
	uint16_t color;

	lcdFillScreen(dev, BLACK);

	strcpy((char *)ascii, "ST7789");
	if (width < height) {
		xpos = ((width - fontHeight) / 2) - 1;
		ypos = (height - (strlen((char *)ascii) * fontWidth)) / 2;
		lcdSetFontDirection(dev, DIRECTION90);
	} else {
		ypos = ((height - fontHeight) / 2) - 1;
		xpos = (width - (strlen((char *)ascii) * fontWidth)) / 2;
		lcdSetFontDirection(dev, DIRECTION0);
	}
	color = WHITE;
	lcdDrawString(dev, fx, xpos, ypos, ascii, color);

	lcdSetFontDirection(dev, 0);
	color = RED;
	lcdDrawFillArrow(dev, 10, 10, 0, 0, 5, color);
	strcpy((char *)ascii, "0,0");
	lcdDrawString(dev, fx, 0, 30, ascii, color);

	color = GREEN;
	lcdDrawFillArrow(dev, width-11, 10, width-1, 0, 5, color);
	//strcpy((char *)ascii, "79,0");
	sprintf((char *)ascii, "%d,0",width-1);
	stlen = strlen((char *)ascii);
	xpos = (width-1) - (fontWidth*stlen);
	lcdDrawString(dev, fx, xpos, 30, ascii, color);

	color = GRAY;
	lcdDrawFillArrow(dev, 10, height-11, 0, height-1, 5, color);
	//strcpy((char *)ascii, "0,159");
	sprintf((char *)ascii, "0,%d",height-1);
	ypos = (height-11) - (fontHeight) + 5;
	lcdDrawString(dev, fx, 0, ypos, ascii, color);

	color = CYAN;
	lcdDrawFillArrow(dev, width-11, height-11, width-1, height-1, 5, color);
	//strcpy((char *)ascii, "79,159");
	sprintf((char *)ascii, "%d,%d",width-1, height-1);
	stlen = strlen((char *)ascii);
	xpos = (width-1) - (fontWidth*stlen);
	lcdDrawString(dev, fx, xpos, ypos, ascii, color);

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t DirectionTest(TFT_t * dev, FontxFile *fx, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	// get font width & height
	uint8_t buffer[FontxGlyphBufSize];
	uint8_t fontWidth;
	uint8_t fontHeight;
	GetFontx(fx, 0, buffer, &fontWidth, &fontHeight);
	//ESP_LOGI(__FUNCTION__,"fontWidth=%d fontHeight=%d",fontWidth,fontHeight);

	uint16_t color;
	lcdFillScreen(dev, BLACK);
	uint8_t ascii[20];

	color = RED;
	strcpy((char *)ascii, "Direction=0");
	lcdSetFontDirection(dev, 0);
	lcdDrawString(dev, fx, 0, fontHeight-1, ascii, color);

	color = BLUE;
	strcpy((char *)ascii, "Direction=2");
	lcdSetFontDirection(dev, 2);
	lcdDrawString(dev, fx, (width-1), (height-1)-(fontHeight*1), ascii, color);

	color = CYAN;
	strcpy((char *)ascii, "Direction=1");
	lcdSetFontDirection(dev, 1);
	lcdDrawString(dev, fx, (width-1)-fontHeight, 0, ascii, color);

	color = GREEN;
	strcpy((char *)ascii, "Direction=3");
	lcdSetFontDirection(dev, 3);
	lcdDrawString(dev, fx, (fontHeight-1), height-1, ascii, color);

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t HorizontalTest(TFT_t * dev, FontxFile *fx, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	// get font width & height
	uint8_t buffer[FontxGlyphBufSize];
	uint8_t fontWidth;
	uint8_t fontHeight;
	GetFontx(fx, 0, buffer, &fontWidth, &fontHeight);
	//ESP_LOGI(__FUNCTION__,"fontWidth=%d fontHeight=%d",fontWidth,fontHeight);

	uint16_t color;
	lcdFillScreen(dev, BLACK);
	uint8_t ascii[20];

	color = RED;
	strcpy((char *)ascii, "Direction=0");
	lcdSetFontDirection(dev, 0);
	lcdDrawString(dev, fx, 0, fontHeight*1-1, ascii, color);
	lcdSetFontUnderLine(dev, RED);
	lcdDrawString(dev, fx, 0, fontHeight*2-1, ascii, color);
	lcdUnsetFontUnderLine(dev);

	lcdSetFontFill(dev, GREEN);
	lcdDrawString(dev, fx, 0, fontHeight*3-1, ascii, color);
	lcdSetFontUnderLine(dev, RED);
	lcdDrawString(dev, fx, 0, fontHeight*4-1, ascii, color);
	lcdUnsetFontFill(dev);
	lcdUnsetFontUnderLine(dev);

	color = BLUE;
	strcpy((char *)ascii, "Direction=2");
	lcdSetFontDirection(dev, 2);
	lcdDrawString(dev, fx, width, height-(fontHeight*1)-1, ascii, color);
	lcdSetFontUnderLine(dev, BLUE);
	lcdDrawString(dev, fx, width, height-(fontHeight*2)-1, ascii, color);
	lcdUnsetFontUnderLine(dev);

	lcdSetFontFill(dev, YELLOW);
	lcdDrawString(dev, fx, width, height-(fontHeight*3)-1, ascii, color);
	lcdSetFontUnderLine(dev, BLUE);
	lcdDrawString(dev, fx, width, height-(fontHeight*4)-1, ascii, color);
	lcdUnsetFontFill(dev);
	lcdUnsetFontUnderLine(dev);

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t VerticalTest(TFT_t * dev, FontxFile *fx, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	// get font width & height
	uint8_t buffer[FontxGlyphBufSize];
	uint8_t fontWidth;
	uint8_t fontHeight;
	GetFontx(fx, 0, buffer, &fontWidth, &fontHeight);
	//ESP_LOGI(__FUNCTION__,"fontWidth=%d fontHeight=%d",fontWidth,fontHeight);

	uint16_t color;
	lcdFillScreen(dev, BLACK);
	uint8_t ascii[20];

	color = RED;
	strcpy((char *)ascii, "Direction=1");
	lcdSetFontDirection(dev, 1);
	lcdDrawString(dev, fx, width-(fontHeight*1), 0, ascii, color);
	lcdSetFontUnderLine(dev, RED);
	lcdDrawString(dev, fx, width-(fontHeight*2), 0, ascii, color);
	lcdUnsetFontUnderLine(dev);

	lcdSetFontFill(dev, GREEN);
	lcdDrawString(dev, fx, width-(fontHeight*3), 0, ascii, color);
	lcdSetFontUnderLine(dev, RED);
	lcdDrawString(dev, fx, width-(fontHeight*4), 0, ascii, color);
	lcdUnsetFontFill(dev);
	lcdUnsetFontUnderLine(dev);

	color = BLUE;
	strcpy((char *)ascii, "Direction=3");
	lcdSetFontDirection(dev, 3);
	lcdDrawString(dev, fx, (fontHeight*1)-1, height, ascii, color);
	lcdSetFontUnderLine(dev, BLUE);
	lcdDrawString(dev, fx, (fontHeight*2)-1, height, ascii, color);
	lcdUnsetFontUnderLine(dev);

	lcdSetFontFill(dev, YELLOW);
	lcdDrawString(dev, fx, (fontHeight*3)-1, height, ascii, color);
	lcdSetFontUnderLine(dev, BLUE);
	lcdDrawString(dev, fx, (fontHeight*4)-1, height, ascii, color);
	lcdUnsetFontFill(dev);
	lcdUnsetFontUnderLine(dev);

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}


TickType_t LineTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	uint16_t color;
	//lcdFillScreen(dev, WHITE);
	lcdFillScreen(dev, BLACK);
	color=RED;
	for(int ypos=0;ypos<height;ypos=ypos+10) {
		lcdDrawLine(dev, 0, ypos, width, ypos, color);
	}

	for(int xpos=0;xpos<width;xpos=xpos+10) {
		lcdDrawLine(dev, xpos, 0, xpos, height, color);
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t CircleTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	uint16_t color;
	//lcdFillScreen(dev, WHITE);
	lcdFillScreen(dev, BLACK);
	color = CYAN;
	uint16_t xpos = width/2;
	uint16_t ypos = height/2;
	for(int i=5;i<height;i=i+5) {
		lcdDrawCircle(dev, xpos, ypos, i, color);
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t RectAngleTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	uint16_t color;
	//lcdFillScreen(dev, WHITE);
	lcdFillScreen(dev, BLACK);
	color = CYAN;
	uint16_t xpos = width/2;
	uint16_t ypos = height/2;

	uint16_t w = width * 0.6;
	uint16_t h = w * 0.5;
	int angle;
	for(angle=0;angle<=(360*3);angle=angle+30) {
		lcdDrawRectAngle(dev, xpos, ypos, w, h, angle, color);
		usleep(10000);
		lcdDrawRectAngle(dev, xpos, ypos, w, h, angle, BLACK);
	}

	for(angle=0;angle<=180;angle=angle+30) {
		lcdDrawRectAngle(dev, xpos, ypos, w, h, angle, color);
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t TriangleTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	uint16_t color;
	//lcdFillScreen(dev, WHITE);
	lcdFillScreen(dev, BLACK);
	color = CYAN;
	uint16_t xpos = width/2;
	uint16_t ypos = height/2;

	uint16_t w = width * 0.6;
	uint16_t h = w * 1.0;
	int angle;

	for(angle=0;angle<=(360*3);angle=angle+30) {
		lcdDrawTriangle(dev, xpos, ypos, w, h, angle, color);
		usleep(10000);
		lcdDrawTriangle(dev, xpos, ypos, w, h, angle, BLACK);
	}

	for(angle=0;angle<=360;angle=angle+30) {
		lcdDrawTriangle(dev, xpos, ypos, w, h, angle, color);
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t RoundRectTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();


	uint16_t color;
	uint16_t limit = width;
	if (width > height) limit = height;
	//lcdFillScreen(dev, WHITE);
	lcdFillScreen(dev, BLACK);
	color = BLUE;
	for(int i=5;i<limit;i=i+5) {
		if (i > (limit-i-1) ) break;
		//ESP_LOGI(__FUNCTION__, "i=%d, width-i-1=%d",i, width-i-1);
		lcdDrawRoundRect(dev, i, i, (width-i-1), (height-i-1), 10, color);
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t FillRectTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	uint16_t color;
	lcdFillScreen(dev, CYAN);

	uint16_t red;
	uint16_t green;
	uint16_t blue;
	srand( (unsigned int)time( NULL ) );
	for(int i=1;i<100;i++) {
		red=rand()%255;
		green=rand()%255;
		blue=rand()%255;
		color=rgb565_conv(red, green, blue);
		uint16_t xpos=rand()%width;
		uint16_t ypos=rand()%height;
		uint16_t size=rand()%(width/5);
		lcdDrawFillRect(dev, xpos, ypos, xpos+size, ypos+size, color);
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

TickType_t ColorTest(TFT_t * dev, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	uint16_t color;
	lcdFillScreen(dev, WHITE);
	color = RED;
	uint16_t delta = height/16;
	uint16_t ypos = 0;
	for(int i=0;i<16;i++) {
		//ESP_LOGI(__FUNCTION__, "color=0x%x",color);
		lcdDrawFillRect(dev, 0, ypos, width-1, ypos+delta, color);
		color = color >> 1;
		ypos = ypos + delta;
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}


TickType_t BMPTest(TFT_t * dev, char * file, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	lcdSetFontDirection(dev, 0);
	// lcdFillScreen(dev, BLACK);

	// open requested file
	esp_err_t ret;
	FILE* fp = fopen(file, "rb");
	if (fp == NULL) {
		ESP_LOGW(__FUNCTION__, "File not found [%s]", file);
		return 0;
	}

	// read bmp header
	bmpfile_t *result = (bmpfile_t*)malloc(sizeof(bmpfile_t));
	ret = fread(result->header.magic, 1, 2, fp);
	assert(ret == 2);
	if (result->header.magic[0]!='B' || result->header.magic[1] != 'M') {
		ESP_LOGW(__FUNCTION__, "File is not BMP");
		free(result);
		fclose(fp);
		return 0;
	}
	ret = fread(&result->header.filesz, 4, 1 , fp);
	assert(ret == 1);
	ESP_LOGD(__FUNCTION__,"result->header.filesz=%d", result->header.filesz);
	ret = fread(&result->header.creator1, 2, 1, fp);
	assert(ret == 1);
	ret = fread(&result->header.creator2, 2, 1, fp);
	assert(ret == 1);
	ret = fread(&result->header.offset, 4, 1, fp);
	assert(ret == 1);

	// read dib header
	ret = fread(&result->dib.header_sz, 4, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.width, 4, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.height, 4, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.nplanes, 2, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.depth, 2, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.compress_type, 4, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.bmp_bytesz, 4, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.hres, 4, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.vres, 4, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.ncolors, 4, 1, fp);
	assert(ret == 1);
	ret = fread(&result->dib.nimpcolors, 4, 1, fp);
	assert(ret == 1);

	if((result->dib.depth == 24) && (result->dib.compress_type == 0)) {
		// BMP rows are padded (if needed) to 4-byte boundary
		uint32_t rowSize = (result->dib.width * 3 + 3) & ~3;
		int w = result->dib.width;
		int h = result->dib.height;
		ESP_LOGD(__FUNCTION__,"w=%d h=%d", w, h);
		int _x;
		int _w;
		int _cols;
		int _cole;
		if (width >= w) {
			_x = (width - w) / 2;
			_w = w;
			_cols = 0;
			_cole = w - 1;
		} else {
			_x = 0;
			_w = width;
			_cols = (w - width) / 2;
			_cole = _cols + width - 1;
		}
		ESP_LOGD(__FUNCTION__,"_x=%d _w=%d _cols=%d _cole=%d",_x, _w, _cols, _cole);

		int _y;
		int _rows;
		int _rowe;
		if (height >= h) {
			_y = (height - h) / 2;
			_rows = 0;
			_rowe = h -1;
		} else {
			_y = 0;
			_rows = (h - height) / 2;
			_rowe = _rows + height - 1;
		}
		ESP_LOGD(__FUNCTION__,"_y=%d _rows=%d _rowe=%d", _y, _rows, _rowe);

#define BUFFPIXEL 20
		uint8_t sdbuffer[3*BUFFPIXEL]; // pixel buffer (R+G+B per pixel)
		uint16_t *colors = (uint16_t*)malloc(sizeof(uint16_t) * w);

		for (int row=0; row<h; row++) { // For each scanline...
			if (row < _rows || row > _rowe) continue;
			// Seek to start of scan line.	It might seem labor-
			// intensive to be doing this on every line, but this
			// method covers a lot of gritty details like cropping
			// and scanline padding.  Also, the seek only takes
			// place if the file position actually needs to change
			// (avoids a lot of cluster math in SD library).
			// Bitmap is stored bottom-to-top order (normal BMP)
			int pos = result->header.offset + (h - 1 - row) * rowSize;
			fseek(fp, pos, SEEK_SET);
			int buffidx = sizeof(sdbuffer); // Force buffer reload

			int index = 0;
			for (int col=0; col<w; col++) { // For each pixel...
				if (buffidx >= sizeof(sdbuffer)) { // Indeed
					fread(sdbuffer, sizeof(sdbuffer), 1, fp);
					buffidx = 0; // Set index to beginning
				}
				if (col < _cols || col > _cole) continue;
				// Convert pixel from BMP to TFT format, push to display
				uint8_t b = sdbuffer[buffidx++];
				uint8_t g = sdbuffer[buffidx++];
				uint8_t r = sdbuffer[buffidx++];
				colors[index++] = rgb565_conv(r, g, b);
			} // end for col
			ESP_LOGD(__FUNCTION__,"lcdDrawMultiPixels _x=%d _y=%d row=%d",_x, _y, row);
			//lcdDrawMultiPixels(dev, _x, row+_y, _w, colors);
			lcdDrawMultiPixels(dev, _x, _y, _w, colors);
			_y++;
		} // end for row
		free(colors);
	} // end if
	free(result);
	fclose(fp);

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}


TickType_t JPEGTest(TFT_t * dev, char * file, int width, int height) {
	TickType_t startTick, endTick, diffTick;
	startTick = xTaskGetTickCount();

	lcdSetFontDirection(dev, 0);
	lcdFillScreen(dev, BLACK);


	pixel_s **pixels;
	uint16_t imageWidth;
	uint16_t imageHeight;
	esp_err_t err = decode_image(&pixels, file, width, height, &imageWidth, &imageHeight);
	ESP_LOGI(__FUNCTION__, "decode_image err=%d imageWidth=%d imageHeight=%d", err, imageWidth, imageHeight);
	if (err == ESP_OK) {

		uint16_t _width = width;
		uint16_t _cols = 0;
		if (width > imageWidth) {
			_width = imageWidth;
			_cols = (width - imageWidth) / 2;
		}
		ESP_LOGD(__FUNCTION__, "_width=%d _cols=%d", _width, _cols);

		uint16_t _height = height;
		uint16_t _rows = 0;
		if (height > imageHeight) {
			_height = imageHeight;
			_rows = (height - imageHeight) / 2;
		}
		ESP_LOGD(__FUNCTION__, "_height=%d _rows=%d", _height, _rows);
		uint16_t *colors = (uint16_t*)malloc(sizeof(uint16_t) * _width);

#if 0
		for(int y = 0; y < _height; y++){
			for(int x = 0;x < _width; x++){
				pixel_s pixel = pixels[y][x];
				uint16_t color = rgb565_conv(pixel.red, pixel.green, pixel.blue);
				lcdDrawPixel(dev, x+_cols, y+_rows, color);
			}
			vTaskDelay(1);
		}
#endif

		for(int y = 0; y < _height; y++){
			for(int x = 0;x < _width; x++){
				pixel_s pixel = pixels[y][x];
				colors[x] = rgb565_conv(pixel.red, pixel.green, pixel.blue);
			}
			lcdDrawMultiPixels(dev, _cols, y+_rows, _width, colors);
			vTaskDelay(1);
		}

		free(colors);
		release_image(&pixels, width, height);
		ESP_LOGD(__FUNCTION__, "Finish");
	}

	endTick = xTaskGetTickCount();
	diffTick = endTick - startTick;
	ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
	return diffTick;
}

void png_init(pngle_t *pngle, uint32_t w, uint32_t h)
{
		ESP_LOGD(__FUNCTION__, "png_init w=%d h=%d", w, h);
		ESP_LOGD(__FUNCTION__, "screenWidth=%d screenHeight=%d", pngle->screenWidth, pngle->screenHeight);
		pngle->imageWidth = w;
		pngle->imageHeight = h;
		pngle->reduction = false;
		pngle->scale_factor = 1.0;

		// Calculate Reduction
		if (pngle->screenWidth < pngle->imageWidth || pngle->screenHeight < pngle->imageHeight) {
				pngle->reduction = true;
				double factorWidth = (double)pngle->screenWidth / (double)pngle->imageWidth;
				double factorHeight = (double)pngle->screenHeight / (double)pngle->imageHeight;
				pngle->scale_factor = factorWidth;
				if (factorHeight < factorWidth) pngle->scale_factor = factorHeight;
				pngle->imageWidth = pngle->imageWidth * pngle->scale_factor;
				pngle->imageHeight = pngle->imageHeight * pngle->scale_factor;
		}
		ESP_LOGD(__FUNCTION__, "reduction=%d scale_factor=%f", pngle->reduction, pngle->scale_factor);
		ESP_LOGD(__FUNCTION__, "imageWidth=%d imageHeight=%d", pngle->imageWidth, pngle->imageHeight);

}

void png_draw(pngle_t *pngle, uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint8_t rgba[4])
{
		ESP_LOGD(__FUNCTION__, "png_draw x=%d y=%d w=%d h=%d", x,y,w,h);
#if 0
		uint8_t r = rgba[0];
		uint8_t g = rgba[1];
		uint8_t b = rgba[2];
#endif

		// image reduction
		uint32_t _x = x;
		uint32_t _y = y;
		if (pngle->reduction) {
				_x = x * pngle->scale_factor;
				_y = y * pngle->scale_factor;
		}
		if (_y < pngle->screenHeight && _x < pngle->screenWidth) {
				pngle->pixels[_y][_x].red = rgba[0];
				pngle->pixels[_y][_x].green = rgba[1];
				pngle->pixels[_y][_x].blue = rgba[2];
		}

}

void png_finish(pngle_t *pngle) {
		ESP_LOGD(__FUNCTION__, "png_finish");
}

TickType_t PNGTest(TFT_t * dev, char * file, int width, int height) {
		TickType_t startTick, endTick, diffTick;
		startTick = xTaskGetTickCount();

		lcdSetFontDirection(dev, 0);
		lcdFillScreen(dev, BLACK);

		// open PNG file
		FILE* fp = fopen(file, "rb");
		if (fp == NULL) {
				ESP_LOGW(__FUNCTION__, "File not found [%s]", file);
				return 0;
		}

		char buf[1024];
		size_t remain = 0;
		int len;

		pngle_t *pngle = pngle_new(width, height);

		pngle_set_init_callback(pngle, png_init);
		pngle_set_draw_callback(pngle, png_draw);
		pngle_set_done_callback(pngle, png_finish);

		double display_gamma = 2.2;
		pngle_set_display_gamma(pngle, display_gamma);


		while (!feof(fp)) {
				if (remain >= sizeof(buf)) {
						ESP_LOGE(__FUNCTION__, "Buffer exceeded");
						while(1) vTaskDelay(1);
				}

				len = fread(buf + remain, 1, sizeof(buf) - remain, fp);
				if (len <= 0) {
						//printf("EOF\n");
						break;
				}

				int fed = pngle_feed(pngle, buf, remain + len);
				if (fed < 0) {
						ESP_LOGE(__FUNCTION__, "ERROR; %s", pngle_error(pngle));
						while(1) vTaskDelay(1);
				}

				remain = remain + len - fed;
				if (remain > 0) memmove(buf, buf + fed, remain);
		}

		fclose(fp);

		uint16_t _width = width;
		uint16_t _cols = 0;
		if (width > pngle->imageWidth) {
				_width = pngle->imageWidth;
				_cols = (width - pngle->imageWidth) / 2;
		}
		ESP_LOGD(__FUNCTION__, "_width=%d _cols=%d", _width, _cols);

		uint16_t _height = height;
		uint16_t _rows = 0;
		if (height > pngle->imageHeight) {
				_height = pngle->imageHeight;
				_rows = (height - pngle->imageHeight) / 2;
		}
		ESP_LOGD(__FUNCTION__, "_height=%d _rows=%d", _height, _rows);
		uint16_t *colors = (uint16_t*)malloc(sizeof(uint16_t) * _width);

#if 0
		for(int y = 0; y < _height; y++){
				for(int x = 0;x < _width; x++){
						pixel_png pixel = pngle->pixels[y][x];
						uint16_t color = rgb565_conv(pixel.red, pixel.green, pixel.blue);
						lcdDrawPixel(dev, x+_cols, y+_rows, color);
				}
		}
#endif

		for(int y = 0; y < _height; y++){
				for(int x = 0;x < _width; x++){
						pixel_png pixel = pngle->pixels[y][x];
						colors[x] = rgb565_conv(pixel.red, pixel.green, pixel.blue);
						//uint16_t color = rgb565_conv(pixel.red, pixel.green, pixel.blue);
						//colors[x] = ~color;
				}
				lcdDrawMultiPixels(dev, _cols, y+_rows, _width, colors);
				vTaskDelay(1);
		}
		free(colors);
		pngle_destroy(pngle, width, height);

		endTick = xTaskGetTickCount();
		diffTick = endTick - startTick;
		ESP_LOGI(__FUNCTION__, "elapsed time[ms]:%d",diffTick*portTICK_RATE_MS);
		return diffTick;
}

void ST7789(void *pvParameters)
{
	// set font file
	FontxFile fx16G[2];
	FontxFile fx24G[2];
	FontxFile fx32G[2];
	InitFontx(fx16G,"/spiffs/ILGH16XB.FNT",""); // 8x16Dot Gothic
	InitFontx(fx24G,"/spiffs/ILGH24XB.FNT",""); // 12x24Dot Gothic
	InitFontx(fx32G,"/spiffs/ILGH32XB.FNT",""); // 16x32Dot Gothic

	FontxFile fx16M[2];
	FontxFile fx24M[2];
	FontxFile fx32M[2];
	InitFontx(fx16M,"/spiffs/ILMH16XB.FNT",""); // 8x16Dot Mincyo
	InitFontx(fx24M,"/spiffs/ILMH24XB.FNT",""); // 12x24Dot Mincyo
	InitFontx(fx32M,"/spiffs/ILMH32XB.FNT",""); // 16x32Dot Mincyo
	
	TFT_t dev;
	spi_master_init(&dev, CONFIG_MOSI_GPIO, CONFIG_SCLK_GPIO, CONFIG_CS_GPIO, CONFIG_DC_GPIO, CONFIG_RESET_GPIO, CONFIG_BL_GPIO);
	lcdInit(&dev, CONFIG_WIDTH, CONFIG_HEIGHT, CONFIG_OFFSETX, CONFIG_OFFSETY);

#if CONFIG_INVERSION
	ESP_LOGI(TAG, "Enable Display Inversion");
	lcdInversionOn(&dev);
#endif

#if 0
	while (1) {
		char file[32];
		strcpy(file, "/spiffs/image.bmp");
		BMPTest(&dev, file, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		strcpy(file, "/spiffs/esp32.jpeg");
		JPEGTest(&dev, file, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

        strcpy(file, "/spiffs/esp_logo.png");
        PNGTest(&dev, file, CONFIG_WIDTH, CONFIG_HEIGHT);
        WAIT;
	}
#endif

#if 0
	//for TEST
	lcdDrawFillRect(&dev, 0, 0, 10, 10, RED);
	lcdDrawFillRect(&dev, 10, 10, 20, 20, GREEN);
	lcdDrawFillRect(&dev, 20, 20, 30, 30, BLUE);
#endif
	lcdBacklightOn(&dev);

	while(1) {

#ifdef ALL_TESTS
		
		FillTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		ColorBarTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		ArrowTest(&dev, fx16G, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		LineTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		CircleTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		RoundRectTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		RectAngleTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		TriangleTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		if (CONFIG_WIDTH >= 240) {
			DirectionTest(&dev, fx24G, CONFIG_WIDTH, CONFIG_HEIGHT);
		} else {
			DirectionTest(&dev, fx16G, CONFIG_WIDTH, CONFIG_HEIGHT);
		}
		WAIT;

		if (CONFIG_WIDTH >= 240) {
			HorizontalTest(&dev, fx24G, CONFIG_WIDTH, CONFIG_HEIGHT);
		} else {
			HorizontalTest(&dev, fx16G, CONFIG_WIDTH, CONFIG_HEIGHT);
		}
		WAIT;

		if (CONFIG_WIDTH >= 240) {
			VerticalTest(&dev, fx24G, CONFIG_WIDTH, CONFIG_HEIGHT);
		} else {
			VerticalTest(&dev, fx16G, CONFIG_WIDTH, CONFIG_HEIGHT);
		}
		WAIT;

		FillRectTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		ColorTest(&dev, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		char file[32];		
		strcpy(file, "/spiffs/image.bmp");
		BMPTest(&dev, file, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		strcpy(file, "/spiffs/esp32.jpeg");
		JPEGTest(&dev, file, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;
#endif
		lcdFillScreen(&dev, BLACK);
		char file[32];		
		strcpy(file, "/spiffs/step.bmp");
		BMPTest(&dev, file, CONFIG_WIDTH, CONFIG_HEIGHT);
		WAIT;

		// Multi Font Test
		uint16_t color;
		uint8_t ascii[40];
		uint16_t margin = 10;
		// lcdFillScreen(&dev, BLACK);
		color = WHITE;
		lcdSetFontDirection(&dev, 0);
		uint16_t xpos = 0;
		uint16_t ypos = 15;
		int xd = 0;
		int yd = 1;
		if(CONFIG_WIDTH < CONFIG_HEIGHT) {
			lcdSetFontDirection(&dev, 1);
			xpos = (CONFIG_WIDTH-1)-16;
			ypos = 0;
			xd = 1;
			yd = 0;
		}
		snprintf((char *)ascii, sizeof(ascii), "hi Rie! %d C %d steps", watch_temperature, watch_step_count);
		lcdDrawString(&dev, fx16G, xpos, ypos, ascii, color);
		

		xpos = xpos - (24 * xd) - (margin * xd);
		ypos = ypos + (16 * yd) + (margin * yd);
		snprintf((char *)ascii, sizeof(ascii), "hi Bjorn: (%d, %d)", touch_x, touch_y);
		lcdDrawString(&dev, fx24G, xpos, ypos, ascii, color);

		xpos = xpos - (32 * xd) - (margin * xd);
		ypos = ypos + (24 * yd) + (margin * yd);
		if (CONFIG_WIDTH >= 240) {
			strcpy((char *)ascii, "32Dot Gothic Font");
			lcdDrawString(&dev, fx32G, xpos, ypos, ascii, color);
			xpos = xpos - (32 * xd) - (margin * xd);;
			ypos = ypos + (32 * yd) + (margin * yd);
		}

		xpos = xpos - (10 * xd) - (margin * xd);
		ypos = ypos + (10 * yd) + (margin * yd);
		strcpy((char *)ascii, "hi Yoko");
		lcdDrawString(&dev, fx16M, xpos, ypos, ascii, color);

		xpos = xpos - (24 * xd) - (margin * xd);;
		ypos = ypos + (16 * yd) + (margin * yd);
		strcpy((char *)ascii, "hi Ryu");
		lcdDrawString(&dev, fx24M, xpos, ypos, ascii, color);

		if (CONFIG_WIDTH >= 240) {
			xpos = xpos - (32 * xd) - (margin * xd);;
			ypos = ypos + (24 * yd) + (margin * yd);
			strcpy((char *)ascii, "32Dot Mincyo Font");
			lcdDrawString(&dev, fx32M, xpos, ypos, ascii, color);
		}
		lcdSetFontDirection(&dev, 0);
		WAIT;

	} // end while

	// never reach
	while (1) {
		vTaskDelay(2000 / portTICK_PERIOD_MS);
	}
}

#define AXP20X_INTR_PIN 35
#define TTGO_SENSOR_I2C_BUS I2C_NUM_0
#define TTGO_SENSOR_I2C_SDA_PIN 21
#define TTGO_SENSOR_I2C_SCL_PIN 22
#define TTGO_SENSOR_I2C_SPEED_HZ 100000

#define TTGO_TOUCH_I2C_BUS I2C_NUM_1
#define TTGO_TOUCH_I2C_SDA_PIN 23
#define TTGO_TOUCH_I2C_SCL_PIN 32
#define TTGO_TOUCH_I2C_SPEED_HZ 100000


#ifdef COMMENT
esp_err_t i2c_master_init(i2c_port_t port) {
	esp_err_t err;
	err = i2c_driver_install(port, I2C_MODE_MASTER, 0, 0, 0);
	if (err != ESP_OK) {
		return err;
	}	
	i2c_config_t i2c_config = {
		.mode = I2C_MODE_MASTER,
		.sda_io_num = SDA_PIN,
		.scl_io_num = SCL_PIN,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = 100000
	};
	return i2c_param_config(port, &i2c_config);
}
#endif

#define AXP20X_CHECK(CHK, TAG, MSG) do {                                       \
	if (CHK != AXP20X_PASS) {                                                  \
            ESP_LOGE(TAG, MSG " failed: %d", CHK);                             \
    }                                                                          \
} while(0)

/*!
 *  @brief Prints the execution status of the APIs.
 */
void bma4_error_codes_print_result(const char api_name[], uint16_t rslt)
{
    if (rslt != BMA4_OK)
    {
        printf("%s\t", api_name);
        if (rslt & BMA4_E_NULL_PTR)
        {
            printf("Error [%d] : Null pointer\r\n", rslt);
        }
        else if (rslt & BMA4_E_CONFIG_STREAM_ERROR)
        {
            printf("Error [%d] : Invalid configuration stream\r\n", rslt);
        }
        else if (rslt & BMA4_E_SELF_TEST_FAIL)
        {
            printf("Error [%d] : Self test failed\r\n", rslt);
        }
        else if (rslt & BMA4_E_INVALID_SENSOR)
        {
            printf("Error [%d] : Device not found\r\n", rslt);
        }
        else
        {
            /* For more error codes refer "*_defs.h" */
            printf("Error [%d] : Unknown error code\r\n", rslt);
        }
    }
}


void test_twali_bma(void * params) {
	struct twali_bma bma = {0};
    // struct bma4_accel_config accel_conf = {0};
    uint16_t rslt = 0;
    // uint8_t dev_addr = BMA4_I2C_ADDR_PRIMARY;
    rslt = twali_bma_begin(&bma, 0);
    bma4_error_codes_print_result("twali_bma_begin status", rslt);
    
    rslt = twali_bma_ttgo_2020_v1_init(&bma);
    bma4_error_codes_print_result("twali_bma_ttgo_2020_v1_init status", rslt);
    
    
    watch_temperature = 0;
    
    rslt = twali_bma_enable_watch_features(&bma);
    bma4_error_codes_print_result("twali_bma_enable_watch_features", rslt);
    
    watch_step_count = twali_bma_get_step_count(&bma);            
    printf("\nThe step counter output is %u\r\n", watch_step_count);
	/* Read temperature. */
	rslt = twali_bma_get_temperature_celcius(&bma, &watch_temperature);
	bma4_error_codes_print_result("twali_bma_get_temperature_celcius status", rslt);
    
    printf("Please interact with the sensor\n");
    while (1) {		
		/* Read temperature. */
		rslt = twali_bma_get_temperature_celcius(&bma, &watch_temperature);
		bma4_error_codes_print_result("twali_bma_get_temperature_celcius status", rslt);
		watch_step_count = twali_bma_get_step_count(&bma);
        // printf("\nThe step counter output is %u\r\n", step_out);
        /* Read the status of the device to check if it has changed. */
        rslt = twali_bma_read_status(&bma);
        bma4_error_codes_print_result("twali_bma_read_irq_status", rslt);
        uint8_t status = twali_bma_status(&bma);
        if (status > 0) {
			printf("\nirq status: %d\r\n", status);
		}
        /* Check if step counter interrupt is triggered */
        if (twali_bma_is_step_count(&bma)) {
            printf("\nStep counter interrupt received\n");
            /* On interrupt, Get step counter output */
            watch_step_count = twali_bma_get_step_count(&bma);            
            printf("\nThe step counter output is %u\r\n", watch_step_count);
        }
        /* Check if tap interrupt is triggered */
        if (twali_bma_is_single_tap(&bma))  {
			printf("\nTAP!\n");
		}
		if (twali_bma_is_wrist_wear(&bma))  {
			printf("\nWRIST!\n");
		}		
		if (twali_bma_is_double_tap(&bma))  {
			printf("\nTAP TAP!\n");
		}
		vTaskDelay(1);
    }

    // printf("\nThe step counter output is %u\r\n", step_out);
}

#define FOCALTECH_SLAVE_ADDRESS    (0x38)

void test_touch(void * params) {
	
	struct ft6236 panel = {0};	
	uint8_t version;
	ft6236_begin(&panel, TTGO_TOUCH_I2C_BUS, FOCALTECH_SLAVE_ADDRESS);
	version = ft6236_get_vendor_id(&panel);
	printf("ft6236_get_vendor_id: %d\n", version);
	ft6236_enable_int(&panel);
	ft6236_set_monitor_time(&panel, 0x0A);
	ft6236_set_monitor_period(&panel, 0x28);
	printf("Please touch the touch panel\n");
    while (1) {
		int i, res;
		struct ft6236_state state = {0};		
		/* Read touches. */
		res = ft6236_get_state(&panel, &state); 
		if (res) {
			printf("Error reading state: %d", res);
			vTaskDelay(100);
		} else if ((state.gesture==0) && (state.event==0) && (state.count==0)) {
			// if (state.touches[0].id!=1) 
			// ignore.
			vTaskDelay(1);
		} else {
			touch_x = state.touches[0].x; 
			touch_y = state.touches[0].y;
			printf("gesture: %d, event: %d, count: %d\n",  state.gesture, state.event, state.count);
			for (i = 0 ; i < state.count; i ++) {
				printf("Touch %d: (id, x,y): (%d, %d,%d)\n", i, state.touches[i].id, state.touches[i].x, state.touches[i].y);
			}		
			vTaskDelay(1);
		}
    }

    // printf("\nThe step counter output is %u\r\n", step_out);
}

void test_rtc(void * params) {
	char buf[32];
	int err;
	struct tm rtc_time = {0};
	struct pcf8563 rtc = {0};	
	pcf8563_begin(&rtc, TTGO_SENSOR_I2C_BUS, PCF8563_SLAVE_ADDRESS);
    while (1) {
		enum pcf8563_alarm_flags alarm_flags;
		memset(&rtc_time, 0, sizeof(rtc_time));
		err = pcf8563_get_time(&rtc, &rtc_time);
		memset(buf, 0, sizeof(buf));		
		strftime(buf, sizeof(buf), "%w %Y-%m-%d %T", &rtc_time);
		printf("RTC time: err %d: %s\n", err, buf);
		memset(&rtc_time, 0, sizeof(rtc_time));
		memset(buf, 0, sizeof(buf));
		alarm_flags = 0;
		err = pcf8563_get_alarm(&rtc, &alarm_flags, &rtc_time);
		strftime(buf, sizeof(buf), "%w %T", &rtc_time);
		printf("RTC alarm: err %d: flags %d: %s\n", err, alarm_flags, buf);
		vTaskDelay(1000);
    }
}

#define SAMPLE_RATE     (16000)
#define I2S_NUM         (0)
#define WAVE_FREQ_HZ    (432)
#define PI              (3.14159265)
#define I2S_BCK_IO      (GPIO_NUM_26)
#define I2S_WS_IO       (GPIO_NUM_25)
#define I2S_DO_IO       (GPIO_NUM_33)
#define I2S_DI_IO       (-1)
#define AUDIO_BITS 		16

#define SAMPLE_PER_CYCLE (SAMPLE_RATE/WAVE_FREQ_HZ)

struct audio_generator {
	int t; // time index. 
	int freq; // frequency
	int16_t *buffer; // buffer
	size_t samples;
	float volume;
};

int note_table[] = {
16 	, // C0			
17 	, // C#0/Db0  	
18 	, // D0			
19 	, //  D#0/Eb0  	
20 	, // E0			
21 	, // F0			
22 	, //  F#0/Gb0  	
24 	, // G0			
25 	, //  G#0/Ab0  	
27 	, // A0			
28 	, //  A#0/Bb0  	
30 	, // B0			
32 	, // C1			
34 	, //  C#1/Db1  	
36 	, // D1			
38 	, //  D#1/Eb1  	
40 	, // E1			
42 	, // F1			
45 	, //  F#1/Gb1  	
48 	, // G1			
50 	, //  G#1/Ab1  	
54 	, // A1			
57 	, //  A#1/Bb1  	
60 	, // B1			
64 	, // C2			
68 	, //  C#2/Db2  	
72 	, // D2			
76 	, //  D#2/Eb2  	
80 	, // E2			
85 	, // F2			
90 	, //  F#2/Gb2  	
96 	, // G2			
101 	, //  G#2/Ab2  	
108 	, // A2			
114 	, //  A#2/Bb2  	
121 	, // B2			
128 	, // C3			
136 	, //  C#3/Db3  	
144 	, // D3			
152 	, //  D#3/Eb3  	
161 	, // E3			
171 	, // F3			
181 	, //  F#3/Gb3  	
192 	, // G3			
203 	, //  G#3/Ab3  	
216 	, // A3			
228 	, //  A#3/Bb3  	
242 	, // B3			
256 	, // C4			
272 	, //  C#4/Db4  	
288 	, // D4			
305 	, //  D#4/Eb4  	
323 	, // E4			
342 	, // F4			
363 	, //  F#4/Gb4  	
384 	, // G4			
407 	, //  G#4/Ab4  	
432 	, // A4			
457 	, //  A#4/Bb4  	
484 	, // B4			
513 	, // C5			
544 	, //  C#5/Db5  	
576 	, // D5			
610 	, //  D#5/Eb5  	
647 	, // E5			
685 	, // F5			
726 	, //  F#5/Gb5  	
769 	, // G5			
815 	, //  G#5/Ab5  	
864 	, // A5			
915 	, //  A#5/Bb5  	
969 	, // B5			
1027 , // C6			
1088 , //  C#6/Db6  	
1153 , // D6			
1221 , //  D#6/Eb6  	
1294 , // E6			
1371 , // F6			
1453 , //  F#6/Gb6  	
1539 , // G6			
1631 , //  G#6/Ab6  	
1728 , // A6			
1830 , //  A#6/Bb6  	
1939 , // B6			
2054 , // C7			
2177 , //  C#7/Db7  	
2306 , // D7			
2443 , //  D#7/Eb7  	
2589 , // E7			
2743 , // F7			
2906 , //  F#7/Gb7  	
3078 , // G7			
3262 , //  G#7/Ab7  	
3456 , // A7			
3661 , //  A#7/Bb7  	
3879 , // B7			
4109 , // C8			
4354 , //  C#8/Db8  	
4613 , // D8			
4887 , //  D#8/Eb8  	
5178 , // E8			
5486 , // F8			
5812 , //  F#8/Gb8  	
6157 , // G8			
6524 , //  G#8/Ab8  	
6912 , // A8			
7323 , //  A#8/Bb8  	
7758 , // B8			
};

int song[] = { 
	 48, 50, 52, 48, -1, 
	 48, 50, 52, 48, -1,
	 52, 53, 55, -1, 
	 52, 53, 55, -1, 
	 55, 57, 55, 53, 52, 48, -1,
	 55, 57, 55, 53, 52, 48, -1,
	 48, 45, 48, -1,
	 48, 45, 48, -1,
	 -1, -1, -1,
};

static void play_sine_wave(struct twali_i2s * dev, struct audio_generator * gen) {	
	// int16_t *samples_data 	= malloc(sizeof(int16_t) * (sample_per_cycle) * 2);
    unsigned int i, sample_val;
    float sin_float 		= 0;
    // size_t i2s_bytes_write = 0;
    // overflow 
    if (gen->t < 0) {
		gen->t = 0;
	}

    for(i = 0; i < (gen->samples * 2); i+=2) {
		if (gen->freq > 0) {
			float time = (float)(gen->t + i) / (float)(SAMPLE_RATE);
			sin_float  = sin(time * 2.0f * PI * (float)(gen->freq));
			sin_float *= (gen->volume * 32767.0);
			sample_val = (int16_t) sin_float;
		} else {
			sample_val = 0;
		}
        gen->buffer[i] = sample_val;
        gen->buffer[i+1] = sample_val;
    }
    gen->t += gen->samples;
    twali_i2s_write_i16(dev, gen->samples, gen->buffer);
	// printf("twali_i2s_write_i16: %d\n", res);
    // free(samples_data);
}


void test_i2s(void * params) {
	int i;
	struct twali_i2s audio;
	int res; 
	int note;
	
	gpio_pad_select_gpio(I2S_BCK_IO);
	gpio_pad_select_gpio(I2S_WS_IO);
	gpio_pad_select_gpio(I2S_DO_IO);
	gpio_set_direction(I2S_BCK_IO, GPIO_MODE_OUTPUT );
	gpio_set_direction(I2S_WS_IO, GPIO_MODE_OUTPUT );
	gpio_set_direction(I2S_DO_IO, GPIO_MODE_OUTPUT );
	
		
	res = twali_i2s_begin(&audio, 0, I2S_BCK_IO, I2S_WS_IO, I2S_DO_IO, I2S_DI_IO,
	SAMPLE_RATE, 16, TWALI_I2S_CHANNEL_RIGHT,  8, 1024);
	
	twali_i2s_set_clk(&audio, SAMPLE_RATE, AUDIO_BITS, 1);
	
	if (res != 0) {
		printf("twali_i2s_begin failed: %d\n", res);
	} else {
		// res = twali_i2s_set_pin(&audio, I2S_BCK_IO, I2S_WS_IO, I2S_DO_IO);
		// printf("twali_i2s_set_pin: %d\n", res);
	}
	#define PUU_SAMPLES (8 * 1024)
	struct audio_generator gen = {
		.t = 0,
		.freq = 200,
		.buffer = malloc(PUU_SAMPLES * sizeof(int16_t) * 2),
		.samples = PUU_SAMPLES,
		.volume = 0.50,
	} ;
	
	i = 0;
	note = song[0];
	if (note < 0) {
		gen.freq = 0;
	} else {
		gen.freq = note_table[note];
	}
	printf("Note: %d %d\n", note, gen.freq);
	while (1) {
		if (gen.t > SAMPLE_RATE) {
			i++;
			if (i >= (sizeof(song)/sizeof(int))) {
				i = 0;
			}
			gen.t = 0;
			note = song[i];
			if (note < 0) {
				gen.freq = 0;
			} else {
				gen.freq = note_table[note];
			}		
			printf("Note: %d %d\n", note, gen.freq);
		}		
		play_sine_wave(&audio, &gen);
	}
}


void app_main(void)
{
	int chk;
	struct axp20x power = {0};
	uint8_t i2c_found[128];
	
	ESP_LOGI(TAG, "Initializing SPIFFS");

	esp_vfs_spiffs_conf_t conf = {
		.base_path = "/spiffs",
		.partition_label = NULL,
		.max_files = 8,
		.format_if_mount_failed =true
	};

	// Use settings defined above toinitialize and mount SPIFFS filesystem.
	// Note: esp_vfs_spiffs_register is anall-in-one convenience function.
	esp_err_t ret =esp_vfs_spiffs_register(&conf);

	if (ret != ESP_OK) {
		if (ret == ESP_FAIL) {
			ESP_LOGE(TAG, "Failed to mount or format filesystem");
		} else if (ret == ESP_ERR_NOT_FOUND) {
			ESP_LOGE(TAG, "Failed to find SPIFFS partition");
		} else {
			ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)",esp_err_to_name(ret));
		}
		return;
	}

	size_t total = 0, used = 0;
	ret = esp_spiffs_info(NULL, &total,&used);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG,"Failed to get SPIFFS partition information (%s)",esp_err_to_name(ret));
	} else {
		ESP_LOGI(TAG,"Partition size: total: %d, used: %d", total, used);
	}

	SPIFFS_Directory("/spiffs/");
	
	ret = twali_i2c_master_init(TTGO_SENSOR_I2C_BUS, TTGO_SENSOR_I2C_SDA_PIN, TTGO_SENSOR_I2C_SCL_PIN, TTGO_SENSOR_I2C_SPEED_HZ);
	if (ret == ESP_FAIL) {
        ESP_LOGE(TAG, "i2c_master_init TTGO_SENSOR_I2C_BUS failed");
    } else {
		ESP_LOGI(TAG, "i2c_master_init OK");
	}
		
	twali_i2c_detect(TTGO_SENSOR_I2C_BUS, i2c_found, true);
	
	ret = twali_i2c_master_init(TTGO_TOUCH_I2C_BUS, TTGO_TOUCH_I2C_SDA_PIN, TTGO_TOUCH_I2C_SCL_PIN, TTGO_TOUCH_I2C_SPEED_HZ);
	if (ret == ESP_FAIL) {
        ESP_LOGE(TAG, "i2c_master_init TTGO_TOUCH_I2C_BUS failed");
    } else {
		ESP_LOGI(TAG, "i2c_master_init OK");
	}
		
	twali_i2c_detect(TTGO_TOUCH_I2C_BUS, i2c_found, true);
	
	
	gpio_pad_select_gpio(CONFIG_BL_GPIO);
    gpio_set_direction(CONFIG_BL_GPIO, GPIO_MODE_OUTPUT);
    // gpio_install_isr_service(); 39
    
    
    chk = axp20x_begin(&power, 0, 0, TTGO_SENSOR_I2C_BUS, AXP202_SLAVE_ADDRESS, false);
	AXP20X_CHECK(chk, TAG, "axp20x_begin");
    if (chk == AXP20X_PASS) {
		//Change the shutdown time to 4 seconds
		chk = axp20x_set_shutdown_time(&power, AXP_POWER_OFF_TIME_4S);
		if (chk != AXP20X_PASS) {
			ESP_LOGE(TAG, "axp20x_set_shutdown_time failed");
		}
		// Turn off the charging instructions, there should be no
		chk =  axp20x_set_chg_ledmode(&power, AXP20X_LED_OFF);
		if (chk != AXP20X_PASS) {
			ESP_LOGE(TAG, "axp20x_set_chg_ledmode failed");
		}
		// Turn off external enable
		chk = axp20x_set_power_output(&power, AXP202_EXTEN, false);		
		if (chk != AXP20X_PASS) {
			ESP_LOGE(TAG, "axp20x_set_power_output failed");
		}
		
		//axp202 allows maximum charging current of 1800mA, minimum 300mA
		chk = axp20x_set_charge_current(&power, 300);
		if (chk != AXP20X_PASS) {
			ESP_LOGE(TAG, "axp20x_set_charge_current failed");
		}
		
		// In the T_WATCH 2020V1 version, the ST7789 chip power supply
		// is shared with the backlight, so LDO2 cannot be turned off
		chk = axp20x_set_power_output(&power, AXP202_LDO2, AXP20X_ON);    
		if (chk != AXP20X_PASS) {
			ESP_LOGE(TAG, "axp20x_set_power_output failed");
		}
		axp20x_debug_charging(&power);
		axp20x_debug_status(&power);
		
		chk = axp20x_set_ldo3_mode(&power, 1);
		if (chk != AXP20X_PASS) {
			ESP_LOGE(TAG, "axp20x_set_ldo3_mode failed");
		}
		chk = axp20x_set_power_output(&power, AXP202_LDO3, AXP20X_ON);
		if (chk != AXP20X_PASS) {
			ESP_LOGE(TAG, "axp20x_set_power_output(&power, AXP202_LDO3, AXP20X_ON) failed");
		}
		
		chk = axp20x_set_ldo3_voltage(&power, 3000);
		if (chk != AXP20X_PASS) {
			ESP_LOGE(TAG, "axp20x_set_ldo3_voltage(&power, 3000) failed");
		}
		
		printf("Battery %d%%, charge: %d\n", axp20x_get_batt_percentage(&power), axp20x_get_charge_current(&power));		
	}
	
	xTaskCreate(test_i2s, "test_i2s", 1024*6, NULL, 2, NULL);
	xTaskCreate(test_rtc, "test_rtc", 1024*6, NULL, 2, NULL);
	xTaskCreate(test_touch, "test_touch", 1024*6, NULL, 2, NULL);
	xTaskCreate(test_twali_bma, "test_twali_bma", 1024*6, NULL, 2, NULL);	
    
	xTaskCreate(ST7789, "ST7789", 1024*6, NULL, 2, NULL);
	
	while (1) {
		printf("app_main\n");
		vTaskDelay(10000);
	}
	
}
