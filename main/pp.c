#include "pp.h"

// This file is for testing the macros in pp.h only.
// It is not intended to be compiled with a C compiler, only tested with cpp.

#define A() 123

A() // Expands to 123
PP_DEFER(A)() // Expands to A () because it requires one more scan to fully expand
PP_EXPAND(PP_DEFER(A)()) // Expands to 123, because the EXPAND macro forces another scan
PP_EXPAND(PP_DEFER(A)()) // Expands to 123, because the EXPAND macro forces another scan

#define LIST_AB PP_LIST_MAKE(a_, b_)
#define LIST_ABCDE  PP_LIST_APPEND(LIST_AB, c_, d_, e_)
#define LIST_EMPTY PP_LIST_MAKE()

LIST_EMPTY
LIST_AB
LIST_ABCDE

PP_HEAD(LIST_AB)
PP_HEAD(LIST_ABCDE)

PP_TAIL(LIST_AB)
PP_TAIL(LIST_ABCDE)

PP_CHOOSE(PP_TRUE(), (a_true), (b_false))
PP_CHOOSE(PP_FALSE(), (a_true), (b_false))

PP_LIST_EMPTY(LIST_EMPTY)
// PP_EMPTY(LIST_ABCDE)

PP_PASTE_2(FOO_, BAR);
PP_PASTE_3(FOO_, PARAM, _BAZ);

#define FOO_1() foo1

PP_PASTE_2(FOO_,1)() 

PP_PASTE_N(2, a_, b_)
PP_PASTE_5(a_, b_, c_, d_, e_)
PP_PASTE_N(5, a_, b_, c_, d_, e_)
PP_PASTE(f, o, o, b, a, r) 


PP_NEED_COMMA()
PP_NEED_COMMA(a_)
PP_NEED_COMMA(a_, b_)

PP_VA_COMMA()
PP_VA_COMMA(a_)
PP_VA_COMMA(a_, b_)


#ifdef PUCWAL_LOG_PRINTF
#define PUCWAL_LOG_WRITE_AID(LEVEL, FMT, ...)                                  \
PUCWAL_LOG_PRINTF("%s:%d:%s:%s: " FMT "\n", __FILE__, __LINE__, __func__, LEVEL, PP_VA_COMMA(__VA_ARGS__))
#else
#define PUCWAL_LOG_WRITE_AID(LEVEL,FMT, ...)                                   \
printf("%s:%d:%s:%s: " FMT "\n", __FILE__, __LINE__, __func__, LEVEL PP_VA_COMMA(__VA_ARGS__))
#endif

#define PUCWAL_LOG_WRITE(LEVEL,FMT, ...) PUCWAL_LOG_WRITE_AID(LEVEL,FMT, __VA_ARGS__)



PUCWAL_LOG_WRITE("DEBUG", "Charge current : %.2f mA\n", 300.0 + val * 100.0);

