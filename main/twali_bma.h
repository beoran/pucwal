#ifndef PUCWAL_BMA_H_INCLUDED
#define PUCWAL_BMA_H_INCLUDED

#include <stdint.h>
#include "bma4_defs.h"
#include "bma4.h"
#include "bma423.h"

enum twali_bma_error { 
	TWALI_BMA_OK			=  0,
	TWALI_BMA_ERROR_RESET	= -1,
	TWALI_BMA_ERROR_INIT	= -2,
	TWALI_BMA_ERROR_CONFIG	= -3,
	TWALI_BMA_ERROR_PIN		= -4,
	TWALI_BMA_ERROR_ACCEL	= -5,
	TWALI_BMA_ERROR_REMAP	= -6,
	TWALI_BMA_ERROR_POWER	= -7,
};

enum twali_motion_direction {
    DIRECTION_TOP_EDGE        = 0,
    DIRECTION_BOTTOM_EDGE     = 1,
    DIRECTION_LEFT_EDGE       = 2,
    DIRECTION_RIGHT_EDGE      = 3,
    DIRECTION_DISP_UP         = 4,
    DIRECTION_DISP_DOWN       = 5
};

typedef struct bma4_dev 			twali_bma_dev_t;
typedef struct bma4_accel 			twali_bma_accel_t;
typedef struct bma4_accel_config 	twali_bma_accel_config_t;
typedef int32_t twali_temperature_celcius;
typedef uint32_t twali_step_count;

struct twali_bma {
	struct { 
		struct bma4_dev dev;
		struct bma4_int_pin_config pin_config;		
		uint16_t bus;
		uint8_t dev_addr;
		uint8_t init;
		uint16_t irq_status;
	} _; /* private data. */
};

#include "twali_bma_proto.h"

#endif
