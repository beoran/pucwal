#ifndef PP_INCLUDED_H
#define PP_INCLUDED_H

// Preprocessing helpers.

#define PP_2STR_AID(A) #A
#define PP_2STR(A) PP_2STR_AID(A)

#define PP_PASTE_AID(A, B) A##B

#define PP_SELECT_32(                                                          \
 _1,  _2,  _3,  _4,  _5,  _6,  _7,  _8,  _9, _10,                              \
_11, _12, _13, _14, _15, _16, _17, _18, _19, _20,                              \
_21, _22, _23, _24, _25, _26, _27, _28, _29, _30,                              \
_31, _32, ...) _32

#define PP_NEED_COMMA(...) PP_SELECT_32(                                       \
__VA_ARGS__, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,                   \
			 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)

#define PP_VA_LEN(...) PP_SELECT_32(                                           \
__VA_ARGS__, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16,   \
			 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)

#define PP_VA_EMPTY(...) PP_SELECT_32(                                         \
__VA_ARGS__, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                   \
			 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)


#define PP_PASTE_2(_1, _2)    PP_PASTE_AID(_1, _2)
#define PP_PASTE_3(_1, _2, _3) PP_PASTE_2(PP_PASTE_2(_1, _2), _3)
#define PP_PASTE_4(_1, _2, _3, _4)                                             \
		PP_PASTE_2(PP_PASTE_3(_1, _2, _3), _4)
#define PP_PASTE_5(_1, _2, _3, _4, _5)                                         \
		PP_PASTE_2(PP_PASTE_4(_1, _2, _3, _4), _5)
#define PP_PASTE_6(_1, _2, _3, _4, _5, _6)                                     \
		PP_PASTE_2(PP_PASTE_5(_1, _2, _3, _4, _5), _6)
#define PP_PASTE_7(_1, _2, _3, _4, _5, _6, _7)                                 \
		PP_PASTE_2(PP_PASTE_6(_1, _2, _3, _4, _5, _6), _7)
#define PP_PASTE_8(_1, _2, _3, _4, _5, _6, _7, _8)                             \
		PP_PASTE_2(PP_PASTE_7(_1, _2, _3, _4, _5, _6, _7), _8)
#define PP_PASTE_9(_1, _2, _3, _4, _5, _6, _7, _8, _9)                         \
		PP_PASTE_2(PP_PASTE_8(_1, _2, _3, _4, _5, _6, _7, _8), _9)
#define PP_PASTE_10(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10)                   \
		PP_PASTE_9(PP_PASTE_8(_1, _2, _3, _4, _5, _6, _7, _8, _9), _10)
#define PP_PASTE_N(N, ...) PP_PASTE_2(PP_PASTE_, N)(__VA_ARGS__)
#define PP_PASTE(...) PP_PASTE_2(PP_PASTE_, PP_VA_LEN(__VA_ARGS__))(__VA_ARGS__)

#define PP_NONE()

#define PP_DEFER(...) __VA_ARGS__ PP_NONE()
#define PP_BLOCK(...) __VA_ARGS__ PP_DEFER(PP_NONE())
#define PP_EXPAND(...) __VA_ARGS__

#define PP_HEAD_AID(_1, ...) _1
#define PP_HEAD(...) PP_HEAD_AID(__VA_ARGS__)
#define PP_TAIL_AID2(HEAD, ...) __VA_ARGS__
#define PP_TAIL_AID(...) PP_TAIL_AID2(__VA_ARGS__)
#define PP_TAIL(...) PP_TAIL_AID(__VA_ARGS__)

/*
#define PP_NONE()
#define PP_DEFER(ID) ID PP_NONE()
#define PP_BLOCK(LIST) LIST PP_DEFER(PP_NONE())
#define PP_EXPAND(LIST) LIST
*/

#define PP_VA_COMMA_1(...) , __VA_ARGS__
#define PP_VA_COMMA_0(...) __VA_ARGS__
#define PP_VA_COMMA(...) PP_PASTE_2(PP_VA_COMMA_, PP_NEED_COMMA(__VA_ARGS__))(__VA_ARGS__)

#define PP_VA_COMMA_AFTER_1(...) __VA_ARGS__ ,
#define PP_VA_COMMA_AFTER_0(...) __VA_ARGS__
#define PP_VA_COMMA_AFTER(...) PP_PASTE_2(PP_VA_COMMA_AFTER_, PP_NEED_COMMA(__VA_ARGS__))(__VA_ARGS__)


#define PP_NIL() 0
#define PP_NIL_AID() PP_NIL()

#define PP_TRUE() 1
#define PP_FALSE() PP_NIL()

#define PP_IS_NIL_AID_0 (1, ())

#define PP_CHOOSE_1(TRUE_LIST, FALSE_LIST) TRUE_LIST
#define PP_CHOOSE_0(TRUE_LIST, FALSE_LIST) FALSE_LIST
#define PP_CHOOSE_(TRUE_LIST, FALSE_LIST) FALSE_LIST
#define PP_CHOOSE(COND, TRUE_LIST, FALSE_LIST)                                 \
    PP_PASTE_2(PP_CHOOSE_, COND)(TRUE_LIST, FALSE_LIST)

#define PP_IS_NIL(VAL) PP_IS_NIL_

#define PP_ATOM_AID(A) (A, PP_NIL())
#define PP_ATOM(A) PP_ATOM_AID(A)


#define PP_LIST_EXPAND_AID(L) PP_EXPAND(PP_EXPAND L)
#define PP_LIST_EXPAND(L) PP_LIST_EXPAND_AID(L)
#define PP_LIST_MAKE_AID(...) (__VA_ARGS__)
#define PP_LIST_MAKE(...) PP_LIST_MAKE_AID(__VA_ARGS__)

#define PP_LIST_PREPEND_AID(L, ...) (PP_VA_COMMA_AFTER(__VA_ARGS__) PP_LIST_EXPAND(L))
#define PP_LIST_PREPEND(L, ...) PP_LIST_PREPEND_AID(L, __VA_ARGS__)

#define PP_LIST_APPEND_AID(L, ...) (PP_LIST_EXPAND(L), PP_VA_COMMA(__VA_ARGS__))
#define PP_LIST_APPEND(L, ...) PP_LIST_APPEND_AID(L, __VA_ARGS__)


#define PP_LIST_EMPTY_AID(LIST) PP_VA_EMPTY(PP_LIST_EXPAND(LIST))
#define PP_LIST_EMPTY(LIST) PP_LIST_EMPTY_AID(LIST)


#endif


