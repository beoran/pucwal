#ifndef FT6236_H_INCLUDED
#define FT6236_H_INCLUDED

#ifdef CPROTO
#else
#include <stdint.h>
#endif


#define FT6236_GEST_ID_MOVE_UP		0x10			
#define FT6236_GEST_ID_MOVE_RIGHT	0x14			
#define FT6236_GEST_ID_MOVE_DOWN	0x18			
#define FT6236_GEST_ID_MOVE_LEFT	0x1C			
#define FT6236_GEST_ID_ZOOM_IN		0x48			
#define FT6236_GEST_ID_ZOOM_OUT		0x49			
#define FT6236_GEST_ID_NONE			0x00			
#define FT6236_TOUCH_ID_PRESS_DOWN	0			
#define FT6236_TOUCH_ID_LIFT_UP		1			
#define FT6236_TOUCH_ID_CONTACT		2			
#define FT6236_TOUCH_ID_NONE		3

#define FT5206_VENDOR_ID            0x11
#define FT6206_CHIP_ID              0x06
#define FT6236_CHIP_ID              0x36
#define FT6236U_CHIP_ID             0x64
#define FT5206U_CHIP_ID             0x64

enum ft6236_gesture {
	ft6236_gesture_move_up		= FT6236_GEST_ID_MOVE_UP		,
	ft6236_gesture_move_right	= FT6236_GEST_ID_MOVE_RIGHT	    ,
	ft6236_gesture_move_down	= FT6236_GEST_ID_MOVE_DOWN	    ,
	ft6236_gesture_move_left	= FT6236_GEST_ID_MOVE_LEFT	    ,
	ft6236_gesture_zoom_in		= FT6236_GEST_ID_ZOOM_IN		,
	ft6236_gesture_zoom_out		= FT6236_GEST_ID_ZOOM_OUT		,
	ft6236_gesture_none			= FT6236_GEST_ID_NONE			,
};

struct ft6236_touch {
	uint8_t id;
	uint8_t kind;
	uint16_t x;
	uint16_t y;
	uint8_t weight;
	uint8_t area;
};

struct ft6236_state {
	uint8_t gesture;
	uint8_t event;
	uint8_t count;
	struct ft6236_touch touches[2];
};

struct ft6236 {
	struct { 
		uint16_t bus;
		uint8_t addr;
		uint8_t init;
		uint16_t irq_status;
	}_;
	struct ft6236_state state;
}; 

#include "ft6236_proto.h"


#endif
