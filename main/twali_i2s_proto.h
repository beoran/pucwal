int twali_i2s_begin(struct twali_i2s *dev, uint8_t i2s_port, uint8_t bck, uint8_t ws, uint8_t data_out, uint8_t data_in, uint32_t sample_rate, uint8_t bits_per_sample, enum twali_i2s_channel channels, uint8_t dma_buffers, uint16_t dma_size);
int twali_i2s_write_u8(struct twali_i2s *dev, size_t size, uint8_t *samples_data);
int twali_i2s_write_i16(struct twali_i2s *dev, size_t size, int16_t *samples_data);
int twali_i2s_zero_dma_buffer(struct twali_i2s *dev);
int twali_i2s_set_clk(struct twali_i2s *dev, uint16_t rate, uint8_t bits, uint8_t channel);
int twali_i2s_set_pin(struct twali_i2s * dev, int bclk, int wclk, int dout);
