/* cmd_i2ctools.c

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>

#include "twali_i2c.h"
#include "twali_log.h"

#ifdef CPROTO
#else
#include "driver/i2c.h"
#include "esp_log.h"
#endif

#define ACK_E 0x1 /*!< I2C master will check ack from slave*/
#define ACK_D 0x0 /*!< I2C master will not check ack from slave */
#define I2C_DELAY (1000 / portTICK_RATE_MS)

#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */
#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */


int twali_i2c_master_init(twali_i2c_port_t port, twali_gpio_pin_t sda, twali_gpio_pin_t sclk, twali_i2c_speed_t speed) {
	esp_err_t err;
	err = i2c_driver_install(port, I2C_MODE_MASTER, 0, 0, 0);
	if (err != ESP_OK) {
		return err;
	}	
	i2c_config_t i2c_config = {
		.mode = I2C_MODE_MASTER,
		.sda_io_num = sda,
		.scl_io_num = sclk,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = speed
	};
	return i2c_param_config(port, &i2c_config);
}


int twali_i2c_get_bytes(twali_i2c_port_t i2c_port, twali_i2c_address_t chip_addr, twali_i2c_register_t data_addr, int len, uint8_t * data) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    if (data_addr != -1) {
        i2c_master_write_byte(cmd, chip_addr << 1 | WRITE_BIT, ACK_CHECK_EN);
        i2c_master_write_byte(cmd, data_addr, ACK_CHECK_EN);
        i2c_master_start(cmd);
    }
    i2c_master_write_byte(cmd, chip_addr << 1 | READ_BIT, ACK_CHECK_EN);
    if (len > 1) {
        i2c_master_read(cmd, data, len - 1, ACK_VAL);
    }
    i2c_master_read_byte(cmd, data + len - 1, NACK_VAL);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, I2C_DELAY);
    i2c_cmd_link_delete(cmd);
    if (ret == ESP_OK) {
		return 0;
    } else if (ret == ESP_ERR_TIMEOUT) {
        PUCWAL_LOG_ERROR("i2c_get_bytes: Bus is busy\n");
        return -1;
    } else {
        PUCWAL_LOG_ERROR("i2c_get_bytes: Read failed: %d %d %d %d %p\n", i2c_port, chip_addr, data_addr, len, data);
        return -2;
    }
}

int twali_i2c_set_bytes(twali_i2c_port_t i2c_port, twali_i2c_address_t chip_addr, twali_i2c_register_t data_addr, int len, uint8_t * data) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, chip_addr << 1 | WRITE_BIT, ACK_CHECK_EN);
    if (data_addr != -1) {
        i2c_master_write_byte(cmd, data_addr, ACK_CHECK_EN);
    }
    for (int i = 0; i < len; i++) {
        i2c_master_write_byte(cmd, data[i], ACK_CHECK_EN);
    }
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret == ESP_OK) {
        return 0;
    } else if (ret == ESP_ERR_TIMEOUT) {
        PUCWAL_LOG_ERROR("i2c_set_bytes: Bus is busy\n");
        return -1;
    } else {
        PUCWAL_LOG_ERROR("i2c_set_bytes: Write Failed: %d %d %d %d %p\n", i2c_port, chip_addr, data_addr, len, data);
        return -2;
    }
}

// Detects available devices on the given i2c port. 
// If debug is nonzero, these are printed out as well.
int twali_i2c_detect(twali_i2c_port_t i2c_port, uint8_t found[128], int8_t debug) {
    uint8_t address;
    if (debug) {
		printf("     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f\r\n");
	}
    int f = 0;
    for (int i = 0; i < 128; i += 16) {
        printf("%02x: ", i);
        for (int j = 0; j < 16; j++) {
            fflush(stdout);
            address = i + j;
            i2c_cmd_handle_t cmd = i2c_cmd_link_create();
            i2c_master_start(cmd);
            i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, 1);
            i2c_master_stop(cmd);
            esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 50 / portTICK_RATE_MS);
            i2c_cmd_link_delete(cmd);
            if (ret == ESP_OK) {
                if (debug) {
					printf("%02x ", address);
				}
                found[f] = address;
                f++;
            } else if (ret == ESP_ERR_TIMEOUT) {
                if (debug) { 
					printf("UU ");
				}
            } else {
				if (debug) { 
					printf("-- ");
				}
            }
        }
        if (debug) { 
			printf("\r\n");
		}
    }
    return 0;
}

