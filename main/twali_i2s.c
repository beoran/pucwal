#include "twali_i2s.h"

#include <math.h>
#include <stdio.h>

#ifndef CPROTO
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/i2s.h"
#include "driver/gpio.h"
#include "esp_system.h"
#endif

int twali_i2s_begin(struct twali_i2s * dev,
uint8_t i2s_port, uint8_t bck, uint8_t ws, uint8_t data_out, uint8_t data_in,
uint32_t sample_rate, uint8_t bits_per_sample, enum twali_i2s_channel channels, 
uint8_t dma_buffers, uint16_t dma_size) {

	dev->i2s_port			= i2s_port;
	dev->bck				= bck;
	dev->ws					= ws;
	dev->data_out			= data_out;
	dev->data_in			= data_in;	
	dev->sample_rate 		= sample_rate;
	dev->bits_per_sample 	= bits_per_sample; 
	dev->channels			= channels;
	dev->dma_buffers 		= dma_buffers;
	dev->dma_size			= dma_size;
	
	/*
	uint8_t channel_format = 0;
	if (dev->channels == TWALI_I2S_CHANNEL_LEFT) {
		channel_format = I2S_CHANNEL_FMT_ALL_LEFT;
	} else if (dev->channels == TWALI_I2S_CHANNEL_RIGHT)  {
		channel_format = I2S_CHANNEL_FMT_ALL_RIGHT;
	} else if (dev->channels == TWALI_I2S_CHANNEL_BOTH)  {
		channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT;
	}*/
	
	i2s_config_t i2s_config = {
		.mode = (I2S_MODE_MASTER | I2S_MODE_TX),
		.sample_rate = dev->sample_rate, 
		.bits_per_sample = dev->bits_per_sample,
		.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
		.communication_format = I2S_COMM_FORMAT_STAND_I2S,
		.intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // default interrupt priority
		.dma_buf_count 	= dev->dma_buffers,
		.dma_buf_len 	= dev->dma_size,
		.use_apll 		= false,
		.tx_desc_auto_clear = true
	};
  
	// i2s pinout
	i2s_pin_config_t pin_config = {
		.bck_io_num = dev->bck,
		.ws_io_num = dev->ws, 
		.data_out_num = dev->data_out, 
		.data_in_num = I2S_PIN_NO_CHANGE
	};
  
	// now configure i2s with constructed pinout and config
	if (i2s_driver_install(dev->i2s_port, &i2s_config, 0, NULL) != ESP_OK) {
		return TWALI_I2S_ERROR_DRIVER;
	}
	
    if (i2s_set_pin(dev->i2s_port, &pin_config) != ESP_OK) {
		return TWALI_I2S_ERROR_PIN;
	}
	
	i2s_set_sample_rates(dev->i2s_port, dev->sample_rate);
	
	return 0;
}


int twali_i2s_write_u8(struct twali_i2s * dev, size_t size, uint8_t * samples_data) {
	size_t bytes_written = 0;  
	esp_err_t err;
	err = i2s_write(dev->i2s_port, samples_data, size, &bytes_written, 100);
	if (err != ESP_OK) {
		return TWALI_I2S_ERROR_WRITE;
	}
	return (int) bytes_written;
}

int twali_i2s_write_i16(struct twali_i2s * dev, size_t size, int16_t * samples_data) {
	size_t bytes_written = 0;  
	esp_err_t err;
	err = i2s_write(dev->i2s_port, samples_data, size * sizeof(int16_t), &bytes_written, 100);
	if (err != ESP_OK) {
		return TWALI_I2S_ERROR_WRITE;
	}
	return (int) bytes_written;
}
   

int twali_i2s_zero_dma_buffer(struct twali_i2s * dev) { 
	esp_err_t err;
	err = i2s_zero_dma_buffer(dev->i2s_port);
	if (err != ESP_OK) {
		return TWALI_I2S_ERROR_ZERO_DMA;
	}
	return 0;
}
 
int twali_i2s_set_clk(struct twali_i2s * dev, uint16_t rate, uint8_t bits, uint8_t channel) { 
	return i2s_set_clk(dev->i2s_port, rate, bits, I2S_CHANNEL_MONO);
}


int twali_i2s_set_pin(struct twali_i2s * dev, int bclk, int wclk, int dout) {
  i2s_pin_config_t pins = {
    .bck_io_num = bclk,
    .ws_io_num = wclk,
    .data_out_num = dout,
    .data_in_num = I2S_PIN_NO_CHANGE
  };
  return i2s_set_pin(dev->i2s_port, &pins);
}




