#include "twali_bma.h"
#include "twali_i2c.h"
#include "twali_time.h"
#include <stdlib.h>

#include <string.h>

static BMA4_INTF_RET_TYPE bma4_reader(uint8_t reg_addr, uint8_t *read_data, uint32_t len, void *intf_ptr) {
	struct twali_bma * b = intf_ptr;
	// I2C_NUM_0, BMA4_I2C_ADDR_SECONDARY
	return twali_i2c_get_bytes(b->_.bus, b->_.dev_addr, reg_addr, len, read_data);
}

static BMA4_INTF_RET_TYPE bma4_writer(uint8_t reg_addr, const uint8_t *read_data, uint32_t len,
                                                void *intf_ptr) {
	struct twali_bma * b = intf_ptr;
	return twali_i2c_set_bytes(b->_.bus, b->_.dev_addr, reg_addr, len, (uint8_t *)read_data);
}

static void bma4_delayer(uint32_t period, void *intf_ptr) {
	twali_time_delay_us(period);
}

int8_t twali_bma_begin_custom(struct twali_bma * b, int bus, int address,
	bma4_read_fptr_t read,
	bma4_write_fptr_t write,
	bma4_delay_us_fptr_t delay_us) {
    if (b->_.init) {
        return 0;
    }
    
    b->_.bus				 = bus;
    b->_.dev_addr            = address;
    b->_.dev.intf			 = BMA4_I2C_INTF;
    b->_.dev.intf_ptr 		 = b;
    /* Assign variant parameter */
    b->_.dev.variant 		 = BMA42X_VARIANT;
    b->_.dev.bus_read        = read;
    b->_.dev.bus_write       = write;
    b->_.dev.delay_us        = delay_us;
    b->_.dev.read_write_len  = 64;
    // b->_.dev.resolution      = 12;
    b->_.dev.feature_len     = BMA423_FEATURE_SIZE;
    
    if (!b->_.dev.bus_read) {
		b->_.dev.bus_read = bma4_reader;
	}
    if (!b->_.dev.bus_write) {
		b->_.dev.bus_write = bma4_writer;
	}
	
    if (!b->_.dev.delay_us) {        
		b->_.dev.delay_us = bma4_delayer;
	}
    
    // Important, the device must be soft reset first and waited for.
	if (twali_bma_reset(b) != BMA4_OK) {
        return TWALI_BMA_ERROR_RESET;
    }
    
    b->_.dev.delay_us(20*1000, b);

	// Now init...
    if (bma423_init(&b->_.dev) != BMA4_OK) {
        return TWALI_BMA_ERROR_INIT;
    }
    
	// and write the firmware.
    if (bma423_write_config_file(&b->_.dev) != BMA4_OK) {
        return TWALI_BMA_ERROR_CONFIG;
    }
    	
	// configure interrupt 1 pin
    b->_.init = 1;
    b->_.pin_config.edge_ctrl = BMA4_LEVEL_TRIGGER;
    b->_.pin_config.lvl = BMA4_ACTIVE_HIGH;
    b->_.pin_config.od = BMA4_PUSH_PULL;
    b->_.pin_config.output_en = BMA4_OUTPUT_ENABLE;
    b->_.pin_config.input_en = BMA4_INPUT_DISABLE;
    if (bma4_set_int_pin_config(&b->_.pin_config, BMA4_INTR1_MAP, &b->_.dev) != BMA4_OK) {
		return TWALI_BMA_ERROR_PIN;    
	}
	b->_.init = 1;
	return TWALI_BMA_OK;
}


/* Liligo TT watch 2020 v1 specific configuration. */
int8_t twali_bma_ttgo_2020_v1_init(struct twali_bma *b) {	
	struct bma4_accel_config accel_conf;
	 /* Accelerometer Configuration Setting */
    /* Output data Rate */
    accel_conf.odr = BMA4_OUTPUT_DATA_RATE_200HZ;

    /* Gravity range of the sensor (+/- 2G, 4G, 8G, 16G) */
    accel_conf.range = BMA4_ACCEL_RANGE_2G;

    /* Bandwidth configure number of sensor samples required to average
     * if value = 2, then 4 samples are averaged
     * averaged samples = 2^(val(accel bandwidth))
     * Note1 : More info refer datasheets
     * Note2 : A higher number of averaged samples will result in a lower noise level of the signal, but since the
     * performance power mode phase is increased, the power consumption will also rise.
     */
    accel_conf.bandwidth = BMA4_ACCEL_NORMAL_AVG4;

    /* Enable the filter performance mode where averaging of samples
     * will be done based on above set bandwidth and ODR.
     * There are two modes
     *  0 -> Averaging samples (Default)
     *  1 -> No averaging
     * For more info on No Averaging mode refer datasheets.
     */
    accel_conf.perf_mode = BMA4_CONTINUOUS_MODE;

    /* Set the accel configurations */
    if (bma4_set_accel_config(&accel_conf, &b->_.dev) != BMA4_OK) {
		return TWALI_BMA_ERROR_ACCEL;
	}	
	b->_.dev.delay_us(10, b);
	
	/* Turn of power saving to enable the step counter. */
	if (bma4_set_advance_power_save(BMA4_DISABLE, &b->_.dev) != BMA4_OK) {
		return TWALI_BMA_ERROR_POWER;
	}

    b->_.dev.delay_us(10, b);

	struct bma423_axes_remap remap_data;
    remap_data.x_axis = 0;
    remap_data.x_axis_sign = 1;
    remap_data.y_axis = 1;
    remap_data.y_axis_sign = 0;
    remap_data.z_axis  = 2;
    remap_data.z_axis_sign  = 1;
    // Need to raise the wrist function, need to set the correct axis
    if (bma423_set_remap_axes(&remap_data, &b->_.dev) != BMA4_OK) {
		return TWALI_BMA_ERROR_REMAP;
	}
	b->_.dev.delay_us(10, b);
	return TWALI_BMA_OK;
}


int8_t twali_bma_begin(struct twali_bma * b, int bus) {
	return twali_bma_begin_custom(b, bus, BMA4_I2C_ADDR_SECONDARY, bma4_reader, 
	bma4_writer, bma4_delayer);
}


int8_t twali_bma_reset(struct twali_bma *b) {
    return bma4_soft_reset(&b->_.dev);
}

int8_t twali_bma_get_accel(struct twali_bma *b, struct bma4_accel *acc) {
    memset(&acc, 0, sizeof(acc));
    return bma4_read_accel_xyz(acc, &b->_.dev);
}

enum twali_motion_direction twali_bma_direction(struct twali_bma *b) {
    struct bma4_accel acc;
    int ret;
    ret = twali_bma_get_accel(b, &acc);
    if (ret != 0) {
        return ret;
    }
    uint16_t absX = abs(acc.x);
    uint16_t absY = abs(acc.y);
    uint16_t absZ = abs(acc.z);

    if ((absZ > absX) && (absZ > absY)) {
        if (acc.z > 0) {
            return DIRECTION_DISP_DOWN;
        } else {
            return DIRECTION_DISP_UP;
        }
    } else if ((absY > absX) && (absY > absZ)) {
        if (acc.y > 0) {
            return DIRECTION_BOTTOM_EDGE;
        } else {
            return  DIRECTION_TOP_EDGE;
        }
    } else {
        if (acc.x < 0) {
            return  DIRECTION_RIGHT_EDGE;
        } else {
            return DIRECTION_LEFT_EDGE;
        }
    }
}

uint8_t twali_bma_get_temperature_celcius(struct twali_bma *b, twali_temperature_celcius * temp) {
    int32_t data = 0;
    uint8_t res = bma4_get_temperature(&data, BMA4_DEG, &b->_.dev);
    if (res != BMA4_OK) {
		return res;
	}
	// somehow the temperature from the bma4 lib is 23 degrees too high.
    *temp = ((twali_temperature_celcius)data / (twali_temperature_celcius) 1000) - 23;
    return res;
}

uint8_t twali_bma_disable_accel(struct twali_bma *b) {
    return twali_bma_set_accel(b, 0);
}

uint8_t twali_bma_enable_accel(struct twali_bma *b) {
    return twali_bma_set_accel(b, 1);
}

uint8_t twali_bma_set_accel(struct twali_bma *b, uint8_t en) {
    return bma4_set_accel_enable(en ? BMA4_ENABLE : BMA4_DISABLE, &b->_.dev);
}

uint8_t twali_bma_set_accel_config(struct twali_bma *b, struct bma4_accel_config ac) {	
    return bma4_set_accel_config(&ac, &b->_.dev);
}

uint8_t twali_bma_disable_irq(struct twali_bma *b, int16_t uint8_t_map) {
    return bma423_map_interrupt(BMA4_INTR1_MAP, uint8_t_map, BMA4_DISABLE, &b->_.dev);
}

uint8_t twali_bma_enable_irq(struct twali_bma *b, int16_t uint8_t_map) {
    return bma423_map_interrupt(BMA4_INTR1_MAP, uint8_t_map, BMA4_ENABLE, &b->_.dev);
}

uint8_t twali_bma_enable_feature(struct twali_bma *b, uint8_t feature, uint8_t enable) {
    if ((feature & BMA423_STEP_CNTR) == BMA423_STEP_CNTR) {
        return bma423_step_detector_enable(enable ? BMA4_ENABLE : BMA4_DISABLE, &b->_.dev);
    }

    return bma423_feature_enable(feature, enable, &b->_.dev);
}

uint8_t twali_bma_reset_step_count(struct twali_bma *b) {
    return bma423_reset_step_counter(&b->_.dev);
}

uint8_t twali_bma_set_step_count_watermark(struct twali_bma *b, uint16_t watermark) {
    return bma423_step_counter_set_watermark(watermark, &b->_.dev);
} 

uint8_t twali_bma_enable_step_count(struct twali_bma *b, uint8_t en) {
	return  twali_bma_enable_feature(b, BMA423_STEP_CNTR, en);
}

uint8_t twali_bma_enable_single_tap(struct twali_bma *b, uint8_t en) {
	return  twali_bma_enable_feature(b, BMA423_SINGLE_TAP, en);
}

uint8_t twali_bma_enable_double_tap(struct twali_bma *b, uint8_t en) {
	return  twali_bma_enable_feature(b, BMA423_DOUBLE_TAP, en);
}

uint8_t twali_bma_enable_wrist_wear(struct twali_bma *b, uint8_t en) {
	return twali_bma_enable_feature(b, BMA423_WRIST_WEAR, en);
}

uint8_t twali_bma_map_interrupt(struct twali_bma *b, uint8_t intr, uint8_t enable) {
	return bma423_map_interrupt(BMA4_INTR1_MAP,  intr, enable, &b->_.dev);
}

uint8_t twali_bma_set_remap_axes(struct twali_bma *b, struct bma423_axes_remap *remap_data) {
    return bma423_set_remap_axes(remap_data, &b->_.dev);
}

uint8_t twali_bma_read_status(struct twali_bma *b) {
    return bma423_read_int_status(&b->_.irq_status, &b->_.dev);
}

uint8_t twali_bma_status(struct twali_bma *b) {
    return b->_.irq_status;
}

twali_step_count twali_bma_get_step_count(struct twali_bma *b) {
    uint32_t step_count;
    int err;
    err = bma423_step_counter_output(&step_count, &b->_.dev);
    if (err == BMA4_OK) {
        return (twali_step_count)(step_count);
    }
    printf("Step count error: %d %d\n", err, step_count);
    return 0;
}

uint8_t twali_bma_is_step_count(struct twali_bma *b) {
    return (BMA423_STEP_CNTR_INT & b->_.irq_status);
}

uint8_t twali_bma_is_wrist_wear(struct twali_bma * b) {
	return (BMA423_WRIST_WEAR_INT & b->_.irq_status);
}

uint8_t twali_bma_is_single_tap(struct twali_bma *b) {
    return (BMA423_SINGLE_TAP_INT & b->_.irq_status);
}

uint8_t twali_bma_is_double_tap(struct twali_bma *b) {
    return (BMA423_DOUBLE_TAP_INT & b->_.irq_status);
}

uint8_t twali_bma_is_activity(struct twali_bma *b) {
    return (BMA423_ACTIVITY_INT & b->_.irq_status);
}

uint8_t twali_bma_is_any_motion(struct twali_bma *b) {
    return (BMA423_ANY_MOT_INT & b->_.irq_status);
}

uint8_t twali_bma_is_no_motion(struct twali_bma *b) {
    return (BMA423_NO_MOT_INT & b->_.irq_status);
}

uint8_t twali_bma_get_activity(struct twali_bma *b) {
    uint8_t activity = 0;
    bma423_activity_output(&activity, &b->_.dev);
    return activity;
}

uint8_t twali_bma_set_wrist_wear_interrupt(struct twali_bma * b, uint8_t enable) {
	return twali_bma_map_interrupt(b, BMA423_WRIST_WEAR_INT, enable);
}

uint8_t twali_bma_set_step_count_interrupt(struct twali_bma * b, uint8_t enable) {
	return twali_bma_map_interrupt(b, BMA423_STEP_CNTR_INT, enable);
}

uint8_t twali_bma_set_single_tap_interrupt(struct twali_bma * b, uint8_t enable) {
	return twali_bma_map_interrupt(b, BMA423_SINGLE_TAP_INT, enable);
}

uint8_t twali_bma_set_double_tap_interrupt(struct twali_bma * b, uint8_t enable) {
	return twali_bma_map_interrupt(b, BMA423_DOUBLE_TAP_INT, enable);
}

uint8_t twali_bma_set_activity_interrupt(struct twali_bma * b, uint8_t enable) {
	return twali_bma_map_interrupt(b, BMA423_ACTIVITY_INT, enable);
}

uint8_t twali_bma_set_any_motion_interrupt(struct twali_bma * b, uint8_t enable) {
	return twali_bma_map_interrupt(b, BMA423_ANY_MOT_INT, enable);
}

uint8_t twali_bma_set_no_motion_interrupt(struct twali_bma * b, uint8_t enable) {
	return twali_bma_map_interrupt(b, BMA423_NO_MOT_INT, enable);
}


/* enables all desirable watch features, namely the accellerator detection 
 * step count, tap, double tap and  wrist motion and their interrupts. */
uint8_t twali_bma_enable_watch_features(struct twali_bma * b) {
	uint8_t res = 0;

	uint8_t intr =  BMA423_WRIST_WEAR_INT | BMA423_STEP_CNTR_INT | 
					BMA423_SINGLE_TAP_INT | BMA423_DOUBLE_TAP_INT;
	
	uint8_t features =   BMA423_STEP_CNTR  |  BMA423_STEP_ACT    |
						 BMA423_WRIST_WEAR |
						 BMA423_SINGLE_TAP | BMA423_DOUBLE_TAP   ;
	
    res |= bma4_set_accel_enable(BMA4_ENABLE, &b->_.dev);
    res |= bma423_feature_enable(features, BMA4_ENABLE, &b->_.dev);
    res |= bma423_step_detector_enable(BMA4_ENABLE, &b->_.dev);
    res |= bma423_step_counter_set_watermark(1, &b->_.dev);
	res |= twali_bma_map_interrupt(b, intr, BMA4_ENABLE);

	return res;
}


