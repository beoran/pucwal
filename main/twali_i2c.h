#ifndef PUCWAL_I2C_H_INCLUDED
#define PUCWAL_I2C_H_INCLUDED

#include <stdint.h>

typedef int8_t twali_gpio_pin_t;
typedef int8_t twali_i2c_port_t;
typedef int8_t twali_i2c_address_t;

typedef int8_t twali_i2c_register_t;
#define PUCWAL_I2C_REGISTER_NONE ((twali_i2c_register_t)(-1))

typedef int32_t twali_i2c_speed_t;

#include "twali_i2c_proto.h"

#endif
