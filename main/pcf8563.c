
/**
 * PCF8563 real-time clock and calendar chip driver for ESP332 and TWALI.
 */
#include "pcf8563.h"
#include "twali_i2c.h"
#include <stdio.h>

#define PCF8563_REG_CONTROL_STATUS_1 	0x00 // TEST1 N STOP N TESTC N N N
#define PCF8563_REG_CONTROL_STATUS_2 	0x01 // N N N TI_TP AF TF AIE TIE
#define PCF8563_REG_VL_SECONDS 			0x02 // VL SECONDS (0 to 59) 
#define PCF8563_REG_MINUTES 			0x03 // x MINUTES (0 to 59) 
#define PCF8563_REG_HOURS 				0x04 // x x HOURS (0 to 23)
#define PCF8563_REG_DAYS 				0x05 // x x DAYS (1 to 31)
#define PCF8563_REG_WEEKDAYS 			0x06 // x x x x
#define PCF8563_REG_CENTURY_MONTHS 		0x07 // C x x MONTHS (1 to 12)
#define PCF8563_REG_YEARS 				0x08 // YEARS (0 to 99)
#define PCF8563_REG_MINUTE_ALARM 		0x09 // AE_M MINUTE_ALARM (0 to 59) 
#define PCF8563_REG_HOUR_ALARM 			0x0A // AE_H x HOUR_ALARM (0 to 23) 
#define PCF8563_REG_DAY_ALARM 			0x0B // AE_D x DAY_ALARM (1 to 31) 
#define PCF8563_REG_WEEKDAY_ALARM 		0x0C // AE_W x x x x WEEKDAY_ALARM (0 to 6)
#define PCF8563_REG_CLKOUT_CONTROL		0x0D // 
#define PCF8563_REG_TIMER_CONTROL 		0x0E // TE
#define PCF8563_REG_TIMER 				0x0F // TIMER[7:0]


#define PCF8563_REG_CONTROL_STATUS_1_STOP 	(1<<5)
#define PCF8563_REG_CONTROL_STATUS_2_TIE  	(1)
#define PCF8563_REG_CONTROL_STATUS_2_AIE  	(1<<1)
#define PCF8563_REG_CONTROL_STATUS_2_TF   	(1<<2)
#define PCF8563_REG_CONTROL_STATUS_2_AF   	(1<<3)
#define PCF8563_REG_CONTROL_STATUS_2_TI_IP 	(1<<4)

#define PCF8563_REG_CENTURY_MONTHS_CENTURY_BIT 	7
#define PCF8563_REG_VL_SECONDS_VL_BIT 			7
#define PCF8563_REG_ALARM_ENABLE_BIT 			7
#define PCF8563_REG_ALARM_MASK					0x7f
#define PCF8563_REG_CLKOUT_CONTROL_FD_BIT 		0
#define PCF8563_REG_CLKOUT_CONTROL_MASK_FD 		0x03
#define PCF8563_REG_CLKOUT_CONTROL_FE_BIT 		7
#define PCF8563_REG_TIMER_CONTROL_MASK_TD 		0x03
#define PCF8563_REG_TIMER_CONTROL_TE_BIT 		7

#define PCF8563_REG_SECONDS_MASK 	0x7f
#define PCF8563_REG_MINUTES_MASK 	0x7f
#define PCF8563_REG_HOURS_MASK 		0x3f
#define PCF8563_REG_DAYS_MASK 		0x3f
#define PCF8563_REG_WEEKDAYS_MASK 	0x7
#define PCF8563_REG_MONTHS_MASK 	0x1f
#define PCF8563_REG_YEARS_MASK 		0x7f



static uint8_t u8_dec2bcd(uint8_t dec) {
	return (((dec / 10) << 4) + (dec % 10));
}

static uint8_t u8_bcd2dec(uint8_t bcd) {
	return (((bcd >> 4) * 10) + (bcd & 0x0f));
}

int pcf8563_begin(struct pcf8563 *p, uint16_t port, uint8_t addr) {
	p->_.bus = port;
	p->_.addr = addr;
	return 0;
}

uint8_t pcf8563_get_register_u8(struct pcf8563 *p, uint8_t reg) {
	uint8_t data;
	twali_i2c_get_bytes(p->_.bus, p->_.addr, reg, 1, &data);
	return data;	
}

int pcf8563_set_register_u8(struct pcf8563 *p, uint8_t reg, uint8_t value) {
	return twali_i2c_set_bytes(p->_.bus, p->_.addr, reg, 1, &value);
}

int pcf8563_read_bytes(struct pcf8563 *p, uint8_t reg, uint8_t *data, uint8_t nbytes) {
	return twali_i2c_get_bytes(p->_.bus, p->_.addr, reg, nbytes, data);
}

int pcf8563_write_bytes(struct pcf8563 *p, uint8_t reg, uint8_t *data, uint8_t nbytes) {
	return twali_i2c_set_bytes(p->_.bus, p->_.addr, reg, nbytes, data);
}

int pcf8563_update_register_u8(struct pcf8563 *p, uint8_t reg, uint8_t mask, uint8_t val) {
    uint8_t v;
    int err;
    v = pcf8563_get_register_u8(p, reg);
    err = pcf8563_set_register_u8(p, reg, (v & ~mask) | val);
    return err;
}

///////////////////////////////////////////////////////////////////////////////

int pcf8563_set_time(struct pcf8563 *p, struct tm *time) {
	// XXX don't care about dates before 2000 or after 2100 for now. 
    uint8_t year = (uint8_t)(time->tm_year - 100);

    uint8_t data[7] = {
        u8_dec2bcd(time->tm_sec),
        u8_dec2bcd(time->tm_min),
        u8_dec2bcd(time->tm_hour),
        u8_dec2bcd(time->tm_wday),
        u8_dec2bcd(time->tm_mday),
        u8_dec2bcd(time->tm_mon + 1),
        u8_dec2bcd(year)
    };

    return pcf8563_write_bytes(p, PCF8563_REG_VL_SECONDS, data, sizeof(data));
}

int pcf8563_get_time(struct pcf8563 *p, struct tm *time) {
    int valid, err;
    uint8_t data[7];

    err = pcf8563_read_bytes(p, PCF8563_REG_VL_SECONDS, data, sizeof(data));
    if (err) {
		return err;
	}
	
	printf("pcf8563_get_time: %x %x %x %x %x %x %x\n", data[0], data[1], data[2], data[3], data[4],
								   data[5], data[6]);
	
    valid 			= data[0] & (1<<PCF8563_REG_VL_SECONDS_VL_BIT);
    time->tm_sec 	= u8_bcd2dec(data[0] & PCF8563_REG_SECONDS_MASK);
    /* The upper unused bits of the time registers may contain crap bits. 
     * Filter them out. */
    time->tm_min 	= u8_bcd2dec(data[1] & PCF8563_REG_MINUTES_MASK);
    time->tm_hour 	= u8_bcd2dec(data[2] & PCF8563_REG_HOURS_MASK);        
    time->tm_mday 	= u8_bcd2dec(data[3] & PCF8563_REG_DAYS_MASK);
    time->tm_wday 	= u8_bcd2dec(data[4] & PCF8563_REG_WEEKDAYS_MASK);    
    time->tm_mon 	= u8_bcd2dec(data[5] & PCF8563_REG_MONTHS_MASK) - 1;
    time->tm_year 	= u8_bcd2dec(data[6] & PCF8563_REG_YEARS_MASK) + (data[5] & (1<<PCF8563_REG_CENTURY_MONTHS_CENTURY_BIT) ? 200 : 100);

    return valid;
}

enum pcf8563_clkout_frequency pcf8563_get_clkout_frequency(struct pcf8563 *p) {
    uint8_t v;
    enum pcf8563_clkout_frequency freq;
    v = pcf8563_get_register_u8(p, PCF8563_REG_CLKOUT_CONTROL);
    
    if (v & (1 << PCF8563_REG_CLKOUT_CONTROL_FE_BIT)) {
		freq = (enum pcf8563_clkout_frequency)((v & PCF8563_REG_CLKOUT_CONTROL_MASK_FD));
	} else {		
		freq = PCF8563_CLKOUT_FREQUENCY_DISABLED;
	}
    return freq;
}

int pcf8563_set_clkout_frequency(struct pcf8563 *p, enum pcf8563_clkout_frequency freq) {
	uint8_t value = 0; 	
	if (freq != PCF8563_CLKOUT_FREQUENCY_DISABLED) {
		value = (1<<PCF8563_REG_CLKOUT_CONTROL_FE_BIT) | 
				(freq & PCF8563_REG_CLKOUT_CONTROL_MASK_FD);
	}	
    return pcf8563_set_register_u8(p, PCF8563_REG_CLKOUT_CONTROL, value);
}

enum pcf8563_timer_frequency pcf8563_get_timer_frequency(struct pcf8563 *p) {
    uint8_t v;
    enum pcf8563_clkout_frequency freq;
    v = pcf8563_get_register_u8(p, PCF8563_REG_TIMER_CONTROL);
    
    if (v & (1 << PCF8563_REG_TIMER_CONTROL_TE_BIT)) {
		freq = (enum pcf8563_timer_frequency)((v & PCF8563_REG_TIMER_CONTROL_MASK_TD));
	} else {
		freq = PCF8563_TIMER_FREQUENCY_DISABLED;
	}
    return freq;
}

int pcf8563_set_timer_frequency(struct pcf8563 *p, enum pcf8563_timer_frequency freq) {
	uint8_t value = 0; 	
	if (freq != PCF8563_TIMER_FREQUENCY_DISABLED) {
		value = (1<<PCF8563_REG_TIMER_CONTROL_TE_BIT) | 
				(freq & PCF8563_REG_TIMER_CONTROL_MASK_TD);
	}	
    return pcf8563_set_register_u8(p, PCF8563_REG_TIMER_CONTROL, value);
}

uint8_t pcf8563_get_timer_value(struct pcf8563 *p) {
    return pcf8563_get_register_u8(p, PCF8563_REG_TIMER);
}

int pcf8563_set_timer_value(struct pcf8563 *p, uint8_t value) {
	return pcf8563_set_register_u8(p, PCF8563_REG_TIMER, value);
}

int pcf8563_start_timer(struct pcf8563 *p) {
    return pcf8563_update_register_u8(p, PCF8563_REG_TIMER_CONTROL,
     1 << PCF8563_REG_TIMER_CONTROL_TE_BIT,
     1 << PCF8563_REG_TIMER_CONTROL_TE_BIT); 
}

int pcf8563_stop_timer(struct pcf8563 *p) {
    return pcf8563_update_register_u8(p, PCF8563_REG_TIMER_CONTROL,
     1 << PCF8563_REG_TIMER_CONTROL_TE_BIT,
     0);
}

int pcf8563_get_timer_flag(struct pcf8563 *p) {
	return pcf8563_get_register_u8(p, PCF8563_REG_CONTROL_STATUS_2) & PCF8563_REG_CONTROL_STATUS_2_TF;
}

int pcf8563_get_alarm_flag(struct pcf8563 *p) {
	return pcf8563_get_register_u8(p, PCF8563_REG_CONTROL_STATUS_2) & PCF8563_REG_CONTROL_STATUS_2_AF;
}

int pcf8563_clear_timer_flag(struct pcf8563 *p) {
    return pcf8563_update_register_u8(p, PCF8563_REG_CONTROL_STATUS_2,
     PCF8563_REG_CONTROL_STATUS_2_TF,
     0);
}

int pcf8563_clear_alarm_flag(struct pcf8563 *p) {
    return pcf8563_update_register_u8(p, PCF8563_REG_CONTROL_STATUS_2,
     PCF8563_REG_CONTROL_STATUS_2_AF,
     0);
}

int pcf8563_enable_alarm_interrupt(struct pcf8563 *p) {
    return pcf8563_update_register_u8(p, PCF8563_REG_CONTROL_STATUS_2,
     PCF8563_REG_CONTROL_STATUS_2_AIE,
     PCF8563_REG_CONTROL_STATUS_2_AIE);
}

int pcf8563_disable_alarm_interrupt(struct pcf8563 *p) {
    return pcf8563_update_register_u8(p, PCF8563_REG_CONTROL_STATUS_2,
     PCF8563_REG_CONTROL_STATUS_2_AIE,
     0);
}

int pcf8563_enable_timer_interrupt(struct pcf8563 *p) {
    return pcf8563_update_register_u8(p, PCF8563_REG_CONTROL_STATUS_2,
     PCF8563_REG_CONTROL_STATUS_2_TIE,
     PCF8563_REG_CONTROL_STATUS_2_TIE);
}

int pcf8563_disable_timer_interrupt(struct pcf8563 *p) {
    return pcf8563_update_register_u8(p, PCF8563_REG_CONTROL_STATUS_2,
     PCF8563_REG_CONTROL_STATUS_2_TIE,
     0);
}

int pcf8563_set_alarm(struct pcf8563 *p, enum pcf8563_alarm_flags flags, struct tm *time) {   
    uint8_t data[4] = {
        u8_dec2bcd(time->tm_min)  | 
			(flags & PCF8563_ALARM_FLAGS_MATCH_MINUTE 	? 0 : (1 << PCF8563_REG_ALARM_ENABLE_BIT)),
        u8_dec2bcd(time->tm_hour) | 
			(flags & PCF8563_ALARM_FLAGS_MATCH_HOUR 	? 0 : (1 << PCF8563_REG_ALARM_ENABLE_BIT)),
        u8_dec2bcd(time->tm_mday) | 
			(flags & PCF8563_ALARM_FLAGS_MATCH_DAY 		? 0 : (1 << PCF8563_REG_ALARM_ENABLE_BIT)),
        u8_dec2bcd(time->tm_wday) | 
			(flags & PCF8563_ALARM_FLAGS_MATCH_WEEKDAY 	? 0 : (1 << PCF8563_REG_ALARM_ENABLE_BIT)),
    };
    return pcf8563_write_bytes(p, PCF8563_REG_MINUTE_ALARM, data, sizeof(data));
}

int pcf8563_get_alarm(struct pcf8563 *p, enum pcf8563_alarm_flags * flags, struct tm *time) {
    uint8_t data[4];
    int err;
	err = pcf8563_read_bytes(p, PCF8563_REG_MINUTE_ALARM, data, sizeof(data));
	if (err) {
		return err;
	}
    *flags = 0;
    if (!(data[0] & (1 << PCF8563_REG_ALARM_ENABLE_BIT)))
        *flags |= PCF8563_ALARM_FLAGS_MATCH_MINUTE;
    if (!(data[1] & (1 << PCF8563_REG_ALARM_ENABLE_BIT)))
        *flags |= PCF8563_ALARM_FLAGS_MATCH_HOUR;
    if (!(data[2] & (1 << PCF8563_REG_ALARM_ENABLE_BIT)))
        *flags |= PCF8563_ALARM_FLAGS_MATCH_DAY;
    if (!(data[3] & (1 << PCF8563_REG_ALARM_ENABLE_BIT)))
        *flags |= PCF8563_ALARM_FLAGS_MATCH_WEEKDAY;

    time->tm_min  = u8_bcd2dec(data[0] & PCF8563_REG_MINUTES_MASK);
    time->tm_hour = u8_bcd2dec(data[1] & PCF8563_REG_HOURS_MASK);
    time->tm_mday = u8_bcd2dec(data[2] & PCF8563_REG_DAYS_MASK);
    time->tm_wday = u8_bcd2dec(data[3] & PCF8563_REG_WEEKDAYS_MASK);

    return 0;
}
