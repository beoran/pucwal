#ifndef TWALI_I2S_H_INCLUDED
#define TWALI_I2S_H_INCLUDED

#include <stdint.h>
#include <stdlib.h>

enum twali_i2s_channel {
	TWALI_I2S_CHANNEL_LEFT  = 1,
	TWALI_I2S_CHANNEL_RIGHT = 2,
	TWALI_I2S_CHANNEL_BOTH  = TWALI_I2S_CHANNEL_LEFT | TWALI_I2S_CHANNEL_RIGHT,
};

#define TWALI_I2S_ERROR_DRIVER 		-1
#define TWALI_I2S_ERROR_PIN 		-2
#define TWALI_I2S_ERROR_WRITE 		-3
#define TWALI_I2S_ERROR_ZERO_DMA 	-4

struct twali_i2s {
	uint8_t i2s_port;
	int8_t bck;
	int8_t ws;
	int8_t data_out;
	int8_t data_in;	
	uint32_t sample_rate;
	uint8_t bits_per_sample; 
	uint8_t dma_buffers;
	uint16_t dma_size;
	enum twali_i2s_channel channels;
};

#include "twali_i2s_proto.h"


#endif

