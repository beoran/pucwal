#ifndef PUCWAL_LOG_H_INCLUDED
#define PUCWAL_LOG_H_INCLUDED

#include "pp.h"

#define PUCWAL_LOG_LEVEL_DEBUG 		50
#define PUCWAL_LOG_LEVEL_INFO  		40
#define PUCWAL_LOG_LEVEL_WARNING  	30
#define PUCWAL_LOG_LEVEL_ERROR  	20
#define PUCWAL_LOG_LEVEL_FATAL  	10

#ifdef NDEBUG
#define PUCWAL_LOG_LEVEL PUCWAL_LOG_LEVEL_WARNING
#else
#define PUCWAL_LOG_LEVEL PUCWAL_LOG_LEVEL_DEBUG
#endif

#ifndef PUCWAL_LOG_PRINTF
#include <stdio.h>
#define PUCWAL_LOG_PRINTF printf
#endif

#define PUCWAL_LOG_WRITE_AID(LEVEL,...)                                        \
do {                                                                           \
	PUCWAL_LOG_PRINTF("%s:%d:%s:%s: " , __FILE__, __LINE__, __func__, LEVEL);  \
	PUCWAL_LOG_PRINTF(__VA_ARGS__);                                            \
	PUCWAL_LOG_PRINTF("\n");                                                   \
} while (0)

#define PUCWAL_LOG_WRITE(LEVEL, ...) PUCWAL_LOG_WRITE_AID(LEVEL, __VA_ARGS__)

#if PUCWAL_LOG_WRITE >= PUCWAL_LOG_WRITE_DEBUG
#define PUCWAL_LOG_DEBUG(...) PUCWAL_LOG_WRITE("D", __VA_ARGS__)
#else
#define PUCWAL_LOG_DEBUG(...)
#endif

#if PUCWAL_LOG_WRITE >= PUCWAL_LOG_WRITE_INFO
#define PUCWAL_LOG_INFO(...) PUCWAL_LOG_WRITE("I", __VA_ARGS__)
#else
#define PUCWAL_LOG_INFO(...)
#endif

#if PUCWAL_LOG_WRITE >= PUCWAL_LOG_WRITE_WARNING
#define PUCWAL_LOG_WARNING(...) PUCWAL_LOG_WRITE("W", __VA_ARGS__)
#else
#define PUCWAL_LOG_WARNING( ...)
#endif

#if PUCWAL_LOG_WRITE >= PUCWAL_LOG_WRITE_ERROR
#define PUCWAL_LOG_ERROR(...) PUCWAL_LOG_WRITE("I", __VA_ARGS__)
#else
#define PUCWAL_LOG_ERROR(...)
#endif

#if PUCWAL_LOG_WRITE >= PUCWAL_LOG_WRITE_FATAL
#define PUCWAL_LOG_FATAL(...) PUCWAL_LOG_WRITE("I",  __VA_ARGS__)
#else
#define PUCWAL_LOG_FATAL(...)
#endif

#endif
