/*
MIT License

Copyright (c) 2020 Beoran
Copyright (c) 2019 lewis he

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

based on axp20x.cpp - Arduino library for X-Power AXP202 chip.
Created by Lewis he on April 1, 2019.
github:https://github.com/lewisxhe/AXP202X_Libraries
*/
#include "driver/i2c.h"
#include "axp20x.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>

/* private macros. */
#define FORCED_OPEN_DCDC3(x) (x |= (AXP20X_ON << AXP202_DCDC3))
#define BIT_MASK(x) (1 << x)
#define IS_OPEN(reg, channel) (bool)(reg & BIT_MASK(channel))
#define _BV(i) AXP20X_BV(i)

static const uint8_t axp20x_startup_params[] = {
    0b00000000,
    0b01000000,
    0b10000000,
    0b11000000
};

static const uint8_t axp20x_long_press_params[] = {
    0b00000000,
    0b00010000,
    0b00100000,
    0b00110000
};

static const uint8_t axp20x_shutdown_params[] = {
    0b00000000,
    0b00000001,
    0b00000010,
    0b00000011
};

static const uint8_t axp20x_target_vol_params[] = {
    0b00000000,
    0b00100000,
    0b01000000,
    0b01100000
};

/*
    int _setGpioInterrupt(uint8_t *val, int mode, bool en);
    int _axp_probe();
    int _axp_irq_mask(axp_gpio_irq_t irq);

    int _axp192_gpio_set(axp_gpio_t gpio, axp_gpio_mode_t mode);
    int _axp192_gpio_0_select( axp_gpio_mode_t mode);
    int _axp192_gpio_1_select( axp_gpio_mode_t mode);
    int _axp192_gpio_3_select( axp_gpio_mode_t mode);
    int _axp192_gpio_4_select( axp_gpio_mode_t mode);

    int _axp202_gpio_set(axp_gpio_t gpio, axp_gpio_mode_t mode);
    int _axp202_gpio_0_select( axp_gpio_mode_t mode);
    int _axp202_gpio_1_select( axp_gpio_mode_t mode);
    int _axp202_gpio_2_select( axp_gpio_mode_t mode);
    int _axp202_gpio_3_select( axp_gpio_mode_t mode);
    int _axp202_gpio_irq_set(axp_gpio_t gpio, axp_gpio_irq_t irq);
    int _axp202_gpio_write(axp_gpio_t gpio, uint8_t val);
    int _axp202_gpio_read(axp_gpio_t gpio);
*/

#define ACK_E 0x1 /*!< I2C master will check ack from slave*/
#define ACK_D 0x0 /*!< I2C master will not check ack from slave */
#define I2C_DELAY (1000 / portTICK_RATE_MS)

#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */
#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */

int i2c_get_bytes(int i2c_port, int chip_addr, int data_addr, int len, uint8_t * data) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    if (data_addr != -1) {
        i2c_master_write_byte(cmd, chip_addr << 1 | WRITE_BIT, ACK_CHECK_EN);
        i2c_master_write_byte(cmd, data_addr, ACK_CHECK_EN);
        i2c_master_start(cmd);
    }
    i2c_master_write_byte(cmd, chip_addr << 1 | READ_BIT, ACK_CHECK_EN);
    if (len > 1) {
        i2c_master_read(cmd, data, len - 1, ACK_VAL);
    }
    i2c_master_read_byte(cmd, data + len - 1, NACK_VAL);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, I2C_DELAY);
    i2c_cmd_link_delete(cmd);
    if (ret == ESP_OK) {
		// AXP20X_DEBUG("i2c_get_bytes: Read OK\n");
		return 0;
    } else if (ret == ESP_ERR_TIMEOUT) {
        AXP20X_DEBUG("i2c_get_bytes: Bus is busy\n");
        return -1;
    } else {
        AXP20X_DEBUG("i2c_get_bytes: Read failed: %d %d %d %d %p\n", i2c_port, chip_addr, data_addr, len, data);
        return -2;
    }
}

int i2c_set_bytes(int i2c_port, int chip_addr, int data_addr, int len, uint8_t * data) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, chip_addr << 1 | WRITE_BIT, ACK_CHECK_EN);
    if (data_addr != -1) {
        i2c_master_write_byte(cmd, data_addr, ACK_CHECK_EN);
    }
    for (int i = 0; i < len; i++) {
        i2c_master_write_byte(cmd, data[i], ACK_CHECK_EN);
    }
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    if (ret == ESP_OK) {
        // AXP20X_DEBUG("i2c_set_bytes: Write OK\n");
        return 0;
    } else if (ret == ESP_ERR_TIMEOUT) {
        AXP20X_DEBUG("i2c_set_bytes: Bus is busy\n");
        return -1;
    } else {
        AXP20X_DEBUG("i2c_set_bytes: Write Failed: %d %d %d %d %p\n", i2c_port, chip_addr, data_addr, len, data);
        return -2;
    }
}

static int _axp20x_read_bytes(struct axp20x *a, uint8_t reg, uint8_t nbytes, uint8_t *data) {    
    if (a->_write_cb) {
        return a->_write_cb(a->_i2c_address, reg, data, nbytes);
    }
    return i2c_get_bytes(a->_i2c_port, a->_i2c_address, reg, nbytes, data); 
}


static int _axp20x_write_bytes(struct axp20x *a, uint8_t reg, uint8_t nbytes, uint8_t *data) {    
    if (a->_write_cb) {
        return a->_write_cb(a->_i2c_address, reg, data, nbytes);
    }
    return i2c_set_bytes(a->_i2c_port, a->_i2c_address, reg, nbytes, data);
}

static int _axp20x_read_byte(struct axp20x *a, uint8_t reg, uint8_t *data) {
	(void) _axp20x_read_byte;
    return _axp20x_read_bytes(a, reg, 1, data);
}

// not exported functions
static uint16_t _axp20x_get_regist_h8_l5(struct axp20x *a, uint8_t regh8, uint8_t regl5) {
    uint8_t hv, lv;
    _axp20x_read_bytes(a, regh8, 1, &hv);
    _axp20x_read_bytes(a, regl5, 1, &lv);
    return (hv << 5) | (lv & 0x1f);
}

static uint16_t _axp20x_get_regist_result(struct axp20x *a, uint8_t regh8, uint8_t regl4) {
    uint8_t hv, lv;
    _axp20x_read_bytes(a, regh8, 1, &hv);
    _axp20x_read_bytes(a, regl4, 1, &lv);
    return (hv << 4) | (lv & 0x0f);
}


int _axp173_axp_probe(struct axp20x * a) {
    uint8_t data;
 
    //!Axp173 does not have a chip ID, read the status register to see if it reads normally
    _axp20x_read_bytes(a, 0x01, 1, &data);
    if (data == 0 || data == 0xFF) {
        return AXP20X_FAIL;
    }
    a->_chip_id = AXP173_CHIP_ID;
    _axp20x_read_bytes(a, AXP202_LDO234_DC23_CTL, 1, &a->_output_reg);
    AXP20X_DEBUG("OUTPUT Register 0x%x\n", a->_output_reg);
    a->_init = true;
    return AXP20X_PASS;
}


int _axp20x_axp_probe(struct axp20x * a) {
	int res = 0;
	AXP20X_DEBUG("_axp20x_axp_probe %p\n", a);
    if (a->_is_axp173) {
        return _axp173_axp_probe(a);
    }
    res = _axp20x_read_bytes(a, AXP202_IC_TYPE, 1, &a->_chip_id);
    AXP20X_DEBUG("chip id detect 0x%x %d\n", a->_chip_id, res);
    if (a->_chip_id == AXP202_CHIP_ID || a->_chip_id == AXP192_CHIP_ID) {
        AXP20X_DEBUG("Detect CHIP :%s\n", a->_chip_id == AXP202_CHIP_ID ? "AXP202" : "AXP192");
        _axp20x_read_bytes(a, AXP202_LDO234_DC23_CTL, 1, &a->_output_reg);
        AXP20X_DEBUG("OUTPUT Register 0x%x\n", a->_output_reg);
        a->_init = true;
        return AXP20X_PASS;
    }
    return AXP20X_FAIL;
}

int axp20x_begin(struct axp20x * a, 
                axp_com_fptr_t read_cb, axp_com_fptr_t write_cb, 
                uint8_t i2cport, uint8_t i2caddr,   bool isAxp173) {
	AXP20X_DEBUG("axp20x_begin started \n");
	AXP20X_DEBUG("axp20x_begin args %p %p %p %d %d %d\n", a, read_cb, write_cb, i2caddr, i2cport, isAxp173);
    if ( (!read_cb || !write_cb) && (!i2caddr)) return AXP20X_FAIL;
    AXP20X_DEBUG("axp20x_begin %p\n", a);
    a->_i2c_address = i2caddr;
    a->_i2c_port    = i2cport;
    a->_read_cb     = read_cb;
    a->_write_cb    = write_cb;
    a->_is_axp173   = isAxp173;    
    return _axp20x_axp_probe(a);
}

bool axp20x_is_dcdc1_enable(struct axp20x * a) {
    if (a->_chip_id == AXP192_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP192_DCDC1);
    else if (a->_chip_id == AXP173_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP173_DCDC1);
    return false;
}

bool axp20x_is_extendable(struct axp20x * a) {
    if (a->_chip_id == AXP192_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP192_EXTEN);
    else if (a->_chip_id == AXP202_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP202_EXTEN);
    else if (a->_chip_id == AXP173_CHIP_ID) {
        uint8_t data;
        _axp20x_read_bytes(a, AXP173_EXTEN_DC2_CTL, 1, &data);
        return IS_OPEN(data, AXP173_CTL_EXTEN_BIT);
    }
    return false;
}

bool axp20x_is_ldo2_enable(struct axp20x * a) {
    if (a->_chip_id == AXP173_CHIP_ID) {
        return IS_OPEN(a->_output_reg, AXP173_LDO2);
    }
    //axp192 same axp202 ldo2 bit
    return IS_OPEN(a->_output_reg, AXP202_LDO2);
}

bool axp20x_is_ldo3_enable(struct axp20x * a) {
    if (a->_chip_id == AXP192_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP192_LDO3);
    else if (a->_chip_id == AXP202_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP202_LDO3);
    else if (a->_chip_id == AXP173_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP173_LDO3);
    return false;
}

bool axp20x_is_ldo4_enable(struct axp20x * a) {
    if (a->_chip_id == AXP202_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP202_LDO4);
    if (a->_chip_id == AXP173_CHIP_ID)
        return IS_OPEN(a->_output_reg, AXP173_LDO4);
    return false;
}
 
bool axp20x_is_dcd2_enable(struct axp20x * a) {
    if (a->_chip_id == AXP173_CHIP_ID) {
        uint8_t data;
        _axp20x_read_bytes(a, AXP173_EXTEN_DC2_CTL, 1, &data);
        return IS_OPEN(data, AXP173_CTL_DC2_BIT);
    }
    /* axp192 and axp202 are the same */ 
    return IS_OPEN(a->_output_reg, AXP202_DCDC2);
}

bool axp20x_is_dcd3_enable(struct axp20x * a) {
    if (a->_chip_id == AXP173_CHIP_ID)
        return false;
    /* axp192 and axp202 are the same */ 
    return IS_OPEN(a->_output_reg, AXP202_DCDC3);
}

int axp20x_set_power_output(struct axp20x * a, uint8_t ch, bool en) {
    uint8_t data;
    uint8_t val = 0;
    if (!a->_init)
        return AXP20X_NOT_INIT;

    //! Axp173 cannot use the REG12H register to control
    //! DC2 and EXTEN. It is necessary to control REG10H separately.
    if (a->_chip_id == AXP173_CHIP_ID) {
        _axp20x_read_bytes(a, AXP173_EXTEN_DC2_CTL, 1, &data);
        if (ch & AXP173_DCDC2) {
            data = en ? data | BIT_MASK(AXP173_CTL_DC2_BIT) : data & (~BIT_MASK(AXP173_CTL_DC2_BIT));
            ch &= (~BIT_MASK(AXP173_DCDC2));
            _axp20x_write_bytes(a, AXP173_EXTEN_DC2_CTL, 1, &data);
        } else if (ch & AXP173_EXTEN) {
            data = en ? data | BIT_MASK(AXP173_CTL_EXTEN_BIT) : data & (~BIT_MASK(AXP173_CTL_EXTEN_BIT));
            ch &= (~BIT_MASK(AXP173_EXTEN));
            _axp20x_write_bytes(a, AXP173_EXTEN_DC2_CTL, 1, &data);
        }
    }

    _axp20x_read_bytes(a, AXP202_LDO234_DC23_CTL, 1, &data);
    if (en) {
        data |= (1 << ch);
    } else {
        data &= (~(1 << ch));
    }

    if (a->_chip_id == AXP202_CHIP_ID) {
        FORCED_OPEN_DCDC3(data); //! Must be forced open in T-Watch
    }

    _axp20x_write_bytes(a, AXP202_LDO234_DC23_CTL, 1, &data);
    /* delay(1) ??? */
    _axp20x_read_bytes(a, AXP202_LDO234_DC23_CTL, 1, &val);
    if (data == val) {
        a->_output_reg = val;
        return AXP20X_PASS;
    }
    return AXP20X_FAIL;
}

bool axp20x_is_charging(struct axp20x * a) {
    uint8_t reg;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, AXP202_MODE_CHGSTATUS, 1, &reg);
    return IS_OPEN(reg, 6);
}

bool axp20x_is_battery_connected(struct axp20x * a) {
    uint8_t reg;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, AXP202_MODE_CHGSTATUS, 1, &reg);
    return IS_OPEN(reg, 5);
}

float axp20x_get_acin_voltage(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_ACIN_VOL_H8, AXP202_ACIN_VOL_L4) * AXP202_ACIN_VOLTAGE_STEP;
}

float axp20x_get_acin_current(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_ACIN_CUR_H8, AXP202_ACIN_CUR_L4) * AXP202_ACIN_CUR_STEP;
}

float axp20x_get_vbus_voltage(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_VBUS_VOL_H8, AXP202_VBUS_VOL_L4) * AXP202_VBUS_VOLTAGE_STEP;
}

float axp20x_get_vbus_current(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_VBUS_CUR_H8, AXP202_VBUS_CUR_L4) * AXP202_VBUS_CUR_STEP;
}

float axp20x_get_temp(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    // Internal temperature
    // 000H => -144.7℃
    // STEP => 0.1℃
    // FFFH => 264.8℃
    return _axp20x_get_regist_result(a, AXP202_INTERNAL_TEMP_H8, AXP202_INTERNAL_TEMP_L4)  * AXP202_INTERNAL_TEMP_STEP  - 144.7;
}

float axp20x_get_tstemp(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_TS_IN_H8, AXP202_TS_IN_L4) * AXP202_TS_PIN_OUT_STEP;
}

float axp20x_get_gpio0_voltage(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_GPIO0_VOL_ADC_H8, AXP202_GPIO0_VOL_ADC_L4) * AXP202_GPIO0_STEP;
}

float axp20x_get_gpio1_voltage(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_GPIO1_VOL_ADC_H8, AXP202_GPIO1_VOL_ADC_L4) * AXP202_GPIO1_STEP;
}

/*
Note: the battery power formula:
Pbat =2* register value * Voltage LSB * Current LSB / 1000.
(Voltage LSB is 1.1mV; Current LSB is 0.5mA, and unit of calculation result is mW.)
*/
float axp20x_get_batt_inpower(struct axp20x * a) {
    float rslt;
    uint8_t hv, mv, lv;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, AXP202_BAT_POWERH8, 1, &hv);
    _axp20x_read_bytes(a, AXP202_BAT_POWERM8, 1, &mv);
    _axp20x_read_bytes(a, AXP202_BAT_POWERL8, 1, &lv);
    rslt = (hv << 16) | (mv << 8) | lv;
    rslt = 2 * rslt * 1.1 * 0.5 / 1000;
    return rslt;
}

float axp20x_get_batt_voltage(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_BAT_AVERVOL_H8, AXP202_BAT_AVERVOL_L4) * AXP202_BATT_VOLTAGE_STEP;
}

float axp20x_get_batt_charge_current(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    switch (a->_chip_id) {
    case AXP202_CHIP_ID:
        return _axp20x_get_regist_result(a, AXP202_BAT_AVERCHGCUR_H8, AXP202_BAT_AVERCHGCUR_L4) * AXP202_BATT_CHARGE_CUR_STEP;
    case AXP192_CHIP_ID:
        return _axp20x_get_regist_h8_l5(a, AXP202_BAT_AVERCHGCUR_H8, AXP202_BAT_AVERCHGCUR_L5) * AXP202_BATT_CHARGE_CUR_STEP;
    default:
        return AXP20X_FAIL;
    }
}

float axp20x_get_batt_discharge_current(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_h8_l5(a, AXP202_BAT_AVERDISCHGCUR_H8, AXP202_BAT_AVERDISCHGCUR_L5) * AXP202_BATT_DISCHARGE_CUR_STEP;
}

float axp20x_get_sys_ipsoutvoltage(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    return _axp20x_get_regist_result(a, AXP202_APS_AVERVOL_H8, AXP202_APS_AVERVOL_L4);
}

/*
Coulomb calculation formula:
C= 65536 * current LSB *（charge coulomb counter value - discharge coulomb counter value） /
3600 / ADC sample rate. Refer to REG84H setting for ADC sample rate；the current LSB is
0.5mA；unit of the calculation result is mAh. ）
*/
uint32_t axp20x_get_batt_charge_coulomb(struct axp20x * a) {
    uint8_t buffer[4];
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, 0xB0, 4, buffer);
    return (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
}

uint32_t axp20x_get_batt_discharge_coulomb(struct axp20x * a) {
    uint8_t buffer[4];
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, 0xB4, 4, buffer);
    return (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
}

float axp20x_get_coulomb_data(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint32_t charge = axp20x_get_batt_charge_coulomb(a), 
             discharge = axp20x_get_batt_discharge_coulomb(a);
    uint8_t rate = axp20x_get_adc_sampling_rate(a);
    float result = 65536.0 * 0.5 * ((float)charge - (float)discharge) / 3600.0 / rate;
    return result;
}

uint8_t axp20x_get_coulomb_register(struct axp20x * a) {
    uint8_t buffer;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, AXP202_COULOMB_CTL, 1, &buffer);
    return buffer;
}


int axp20x_set_coulomb_register(struct axp20x * a, uint8_t val) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_write_bytes(a, AXP202_COULOMB_CTL, 1, &val);
    return AXP20X_PASS;
}


int axp20x_enable_coulomb_counter(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val = 0x80;
    _axp20x_write_bytes(a, AXP202_COULOMB_CTL, 1, &val);
    return AXP20X_PASS;
}

int axp20x_disable_coulomb_counter(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val = 0x00;
    _axp20x_write_bytes(a, AXP202_COULOMB_CTL, 1, &val);
    return AXP20X_PASS;
}

int axp20x_stop_coulomb_counter(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val = 0xB8;
    _axp20x_write_bytes(a, AXP202_COULOMB_CTL, 1, &val);
    return AXP20X_PASS;
}


int axp20x_clear_coulomb_counter(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val = 0xA0;
    _axp20x_write_bytes(a, AXP202_COULOMB_CTL, 1, &val);
    return AXP20X_PASS;
}

uint8_t axp20x_get_adc_sampling_rate(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_ADC_SPEED, 1, &val);
    return 25 * (int)pow(2, (val & 0xC0) >> 6);
}

int axp20x_set_adc_sampling_rate(struct axp20x * a, axp_adc_sampling_rate_t rate) {
    //axp192 same axp202 aregister address 0x84
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (rate > AXP20X_ADC_SAMPLING_RATE_200HZ)
        return AXP20X_FAIL;
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_ADC_SPEED, 1, &val);
    uint8_t rw = rate;
    val &= 0x3F;
    val |= (rw << 6);
    _axp20x_write_bytes(a, AXP202_ADC_SPEED, 1, &val);
    return AXP20X_PASS;
}

int axp20x_set_tsfunction(struct axp20x * a, axp_ts_pin_function_t func) {
    //axp192 same axp202 aregister address 0x84
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (func > AXP20X_TS_PIN_FUNCTION_ADC)
        return AXP20X_FAIL;
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_ADC_SPEED, 1, &val);
    uint8_t rw = func;
    val &= 0xFA;
    val |= (rw << 2);
    _axp20x_write_bytes(a, AXP202_ADC_SPEED, 1, &val);
    return AXP20X_PASS;
}

int axp20x_set_tsc(struct axp20x * a, axp_ts_pin_current_t current) {
     if (!a->_init)
        return AXP20X_NOT_INIT;
    if (current > AXP20X_TS_PIN_CURRENT_80UA)
        return AXP20X_FAIL;
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_ADC_SPEED, 1, &val);
    uint8_t rw = current;
    val &= 0xCF;
    val |= (rw << 4);
    _axp20x_write_bytes(a, AXP202_ADC_SPEED, 1, &val);
    return AXP20X_PASS;
}

int axp20x_adc1_enable(struct axp20x * a, uint16_t params, bool en) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_ADC_EN1, 1, &val);
    if (en)
        val |= params;
    else
        val &= ~(params);
    _axp20x_write_bytes(a, AXP202_ADC_EN1, 1, &val);
    return AXP20X_PASS;
}

int axp20x_adc2_enable(struct axp20x * a, uint16_t params, bool en) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_ADC_EN2, 1, &val);
    if (en)
        val |= params;
    else
        val &= ~(params);
    _axp20x_write_bytes(a, AXP202_ADC_EN2, 1, &val);
    return AXP20X_PASS;
}


int axp20x_set_tsmode(struct axp20x * a, axp_ts_pin_mode_t mode) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (mode > AXP20X_TS_PIN_MODE_ENABLE)
        return AXP20X_FAIL;
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_ADC_SPEED, 1, &val);
    uint8_t rw = mode;
    val &= 0xFC;
    val |= rw;
    _axp20x_write_bytes(a, AXP202_ADC_SPEED, 1, &val);

    // TS pin ADC function enable/disable
    if (mode == AXP20X_TS_PIN_MODE_DISABLE)
        axp20x_adc1_enable(a, AXP202_TS_PIN_ADC1, false);
    else
        axp20x_adc1_enable(a, AXP202_TS_PIN_ADC1, true);
    return AXP20X_PASS;
}


int axp20x_enable_irq(struct axp20x * a, uint64_t params, bool en) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val, val1;
    if (params & 0xFF) {
        val1 = params & 0xFF;
        _axp20x_read_bytes(a, AXP202_INTEN1, 1, &val);
        if (en)
            val |= val1;
        else
            val &= ~(val1);
        AXP20X_DEBUG("%s [0x%x]val:0x%x\n", en ? "enable" : "disable", AXP202_INTEN1, val);
        _axp20x_write_bytes(a, AXP202_INTEN1, 1, &val);
    }
    if (params & 0xFF00) {
        val1 = params >> 8;
        _axp20x_read_bytes(a, AXP202_INTEN2, 1, &val);
        if (en)
            val |= val1;
        else
            val &= ~(val1);
        AXP20X_DEBUG("%s [0x%x]val:0x%x\n", en ? "enable" : "disable", AXP202_INTEN2, val);
        _axp20x_write_bytes(a, AXP202_INTEN2, 1, &val);
    }

    if (params & 0xFF0000) {
        val1 = params >> 16;
        _axp20x_read_bytes(a, AXP202_INTEN3, 1, &val);
        if (en)
            val |= val1;
        else
            val &= ~(val1);
        AXP20X_DEBUG("%s [0x%x]val:0x%x\n", en ? "enable" : "disable", AXP202_INTEN3, val);
        _axp20x_write_bytes(a, AXP202_INTEN3, 1, &val);
    }

    if (params & 0xFF000000) {
        val1 = params >> 24;
        _axp20x_read_bytes(a, AXP202_INTEN4, 1, &val);
        if (en)
            val |= val1;
        else
            val &= ~(val1);
        AXP20X_DEBUG("%s [0x%x]val:0x%x\n", en ? "enable" : "disable", AXP202_INTEN4, val);
        _axp20x_write_bytes(a, AXP202_INTEN4, 1, &val);
    }

    if (params & 0xFF00000000) {
        val1 = params >> 32;
        _axp20x_read_bytes(a, AXP202_INTEN5, 1, &val);
        if (en)
            val |= val1;
        else
            val &= ~(val1);
        AXP20X_DEBUG("%s [0x%x]val:0x%x\n", en ? "enable" : "disable", AXP202_INTEN5, val);
        _axp20x_write_bytes(a, AXP202_INTEN5, 1, &val);
    }
    return AXP20X_PASS;
}

int axp20x_read_irq(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    switch (a->_chip_id) {
    case AXP192_CHIP_ID:
        for (int i = 0; i < 4; ++i) {
            _axp20x_read_bytes(a, AXP192_INTSTS1 + i, 1, &a->_irq[i]);
        }
        _axp20x_read_bytes(a, AXP192_INTSTS5, 1, &a->_irq[4]);
        return AXP20X_PASS;

    case AXP202_CHIP_ID:
        for (int i = 0; i < 5; ++i) {
            _axp20x_read_bytes(a, AXP202_INTSTS1 + i, 1, &a->_irq[i]);
        }
        return AXP20X_PASS;
    default:
        return AXP20X_FAIL;
    }
}

void axp20x_clear_irq(struct axp20x * a) {
    uint8_t val = 0xFF;
    switch (a->_chip_id) {
    case AXP192_CHIP_ID:
        for (int i = 0; i < 3; i++) {
            _axp20x_write_bytes(a, AXP192_INTSTS1 + i, 1, &val);
        }
        _axp20x_write_bytes(a, AXP192_INTSTS5, 1, &val);
        break;
    case AXP202_CHIP_ID:
        for (int i = 0; i < 5; i++) {
            _axp20x_write_bytes(a, AXP202_INTSTS1 + i, 1, &val);
        }
        break;
    default:
        break;
    }
    memset(a->_irq, 0, sizeof(a->_irq));
}

bool axp20x_is_acinovervoltage_irq(struct axp20x * a) {
    return (bool)(a->_irq[0] & BIT_MASK(7));
}

bool axp20x_is_acinplugin_irq(struct axp20x * a) {
    return (bool)(a->_irq[0] & BIT_MASK(6));
}

bool axp20x_is_acinremove_irq(struct axp20x * a) {
    return (bool)(a->_irq[0] & BIT_MASK(5));
}

bool axp20x_is_vbusovervoltage_irq(struct axp20x * a) {
    return (bool)(a->_irq[0] & BIT_MASK(4));
}

bool axp20x_is_vbusplugin_irq(struct axp20x * a) {
    return (bool)(a->_irq[0] & BIT_MASK(3));
}

bool axp20x_is_vbusremove_irq(struct axp20x * a) {
    return (bool)(a->_irq[0] & BIT_MASK(2));
}

bool axp20x_is_vbuslowvhold_irq(struct axp20x * a) {
    return (bool)(a->_irq[0] & BIT_MASK(1));
}

bool axp20x_is_batt_plugin_irq(struct axp20x * a) {
    return (bool)(a->_irq[1] & BIT_MASK(7));
}
bool axp20x_is_batt_remove_irq(struct axp20x * a) {
    return (bool)(a->_irq[1] & BIT_MASK(6));
}
bool axp20x_is_batt_enter_activate_irq(struct axp20x * a) {
    return (bool)(a->_irq[1] & BIT_MASK(5));
}
bool axp20x_is_batt_exit_activate_irq(struct axp20x * a) {
    return (bool)(a->_irq[1] & BIT_MASK(4));
}
bool axp20x_is_charging_irq(struct axp20x * a) {
    return (bool)(a->_irq[1] & BIT_MASK(3));
}
bool axp20x_is_charging_done_irq(struct axp20x * a) {
    return (bool)(a->_irq[1] & BIT_MASK(2));
}
bool axp20x_is_batt_temp_low_irq(struct axp20x * a) {
    return (bool)(a->_irq[1] & BIT_MASK(1));
}
bool axp20x_is_batt_temp_high_irq(struct axp20x * a) {
    return (bool)(a->_irq[1] & BIT_MASK(0));
}

bool axp20x_is_pek_short_press_irq(struct axp20x * a) {
    return (bool)(a->_irq[2] & BIT_MASK(1));
}

bool axp20x_is_pek_long_press_irq(struct axp20x * a) {
    return (bool)(a->_irq[2] & BIT_MASK(0));
}

bool axp20x_is_timer_timeout_irq(struct axp20x * a) {
    return (bool)(a->_irq[4] & BIT_MASK(7));
}

bool axp20x_is_vbus_plug(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t reg;
    _axp20x_read_bytes(a, AXP202_STATUS, 1, &reg);
    return IS_OPEN(reg, 5);
}

int axp20x_set_dcdc2_voltage(struct axp20x * a, axp20x_millivolt mv) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (mv < 700) {
        AXP20X_DEBUG("DCDC2:Below settable voltage:700mV~2275mV");
        mv = 700;
    }
    if (mv > 2275) {
        AXP20X_DEBUG("DCDC2:Above settable voltage:700mV~2275mV");
        mv = 2275;
    }
    uint8_t val = (mv - 700) / 25;
    _axp20x_write_bytes(a, AXP202_DC2OUT_VOL, 1, &val);
    return AXP20X_PASS;
}

axp20x_millivolt axp20x_get_dcdc2_voltage(struct axp20x * a) {
    uint8_t val = 0;
    _axp20x_read_bytes(a, AXP202_DC2OUT_VOL, 1, &val);
    return (axp20x_millivolt)(val * 25 + 700);
}

axp20x_millivolt axp20x_get_dcdc3_voltage(struct axp20x * a) {
    if (!a->_init)
        return 0;
    if (a->_chip_id == AXP173_CHIP_ID) {
        return AXP20X_NOT_SUPPORT;
    }
    uint8_t val = 0;
    _axp20x_read_bytes(a, AXP202_DC3OUT_VOL, 1, &val);
    return (axp20x_millivolt)(val * 25 + 700);
}

int axp20x_set_dcdc3_voltage(struct axp20x * a, axp20x_millivolt mv) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id == AXP173_CHIP_ID){ 
        return AXP20X_NOT_SUPPORT;
    }
    if (mv < 700) {
        AXP20X_DEBUG("DCDC3:Below settable voltage:700mV~3500mV");
        mv = 700;
    }
    if (mv > 3500) {
        AXP20X_DEBUG("DCDC3:Above settable voltage:700mV~3500mV");
        mv = 3500;
    }
    uint8_t val = (mv - 700) / 25;
    _axp20x_write_bytes(a, AXP202_DC3OUT_VOL, 1, &val);
    return AXP20X_PASS;
}

int axp20x_set_ldo2_voltage(struct axp20x * a, axp20x_millivolt mv) {
    uint8_t rVal, wVal;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (mv < 1800) {
        AXP20X_DEBUG("LDO2:Below settable voltage:1800mV~3300mV");
        mv = 1800;
    }
    if (mv > 3300) {
        AXP20X_DEBUG("LDO2:Above settable voltage:1800mV~3300mV");
        mv = 3300;
    }
    wVal = (mv - 1800) / 100;
    if (a->_chip_id == AXP202_CHIP_ID) {
        _axp20x_read_bytes(a, AXP202_LDO24OUT_VOL, 1, &rVal);
        rVal &= 0x0F;
        rVal |= (wVal << 4);
        _axp20x_write_bytes(a, AXP202_LDO24OUT_VOL, 1, &rVal);
        return AXP20X_PASS;
    } else if (a->_chip_id == AXP192_CHIP_ID || a->_chip_id == AXP173_CHIP_ID) {
        _axp20x_read_bytes(a, AXP192_LDO23OUT_VOL, 1, &rVal);
        rVal &= 0x0F;
        rVal |= (wVal << 4);
        _axp20x_write_bytes(a, AXP192_LDO23OUT_VOL, 1, &rVal);
        return AXP20X_PASS;
    }
    return AXP20X_FAIL;
}

axp20x_millivolt axp20x_get_ldo2_voltage(struct axp20x * a) {
    uint8_t rVal;
    if (a->_chip_id == AXP202_CHIP_ID) {
        _axp20x_read_bytes(a, AXP202_LDO24OUT_VOL, 1, &rVal);
        rVal &= 0xF0;
        rVal >>= 4;
        return (axp20x_millivolt)(rVal * 100 + 1800);
    } else if (a->_chip_id == AXP192_CHIP_ID || a->_chip_id == AXP173_CHIP_ID ) {
        _axp20x_read_bytes(a, AXP192_LDO23OUT_VOL, 1, &rVal);
        AXP20X_DEBUG("get result:%x\n", rVal);
        rVal &= 0xF0;
        rVal >>= 4;
        return (axp20x_millivolt)(rVal * 100 + 1800);
    }
    return (axp20x_millivolt)(0);
}

int axp20x_set_ldo3_voltage(struct axp20x * a, axp20x_millivolt mv) {
    uint8_t rVal;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id == AXP202_CHIP_ID && mv < 700) {
        AXP20X_DEBUG("LDO3:Below settable voltage:700mV~3500mV");
        mv = 700;
    } else if (a->_chip_id == AXP192_CHIP_ID && mv < 1800) {
        AXP20X_DEBUG("LDO3:Below settable voltage:1800mV~3300mV");
        mv = 1800;
    }

    if (a->_chip_id == AXP202_CHIP_ID && mv > 3500) {
        AXP20X_DEBUG("LDO3:Above settable voltage:700mV~3500mV");
        mv = 3500;
    } else if (a->_chip_id == AXP192_CHIP_ID && mv > 3300) {
        AXP20X_DEBUG("LDO3:Above settable voltage:1800mV~3300mV");
        mv = 3300;
    }

    if (a->_chip_id == AXP202_CHIP_ID) {
        _axp20x_read_bytes(a, AXP202_LDO3OUT_VOL, 1, &rVal);
        rVal &= 0x80;
        rVal |= ((mv - 700) / 25);
        _axp20x_write_bytes(a, AXP202_LDO3OUT_VOL, 1, &rVal);
        return AXP20X_PASS;
    } else if (a->_chip_id == AXP192_CHIP_ID || a->_chip_id == AXP173_CHIP_ID) {
        _axp20x_read_bytes(a, AXP192_LDO23OUT_VOL, 1, &rVal);
        rVal &= 0xF0;
        rVal |= ((mv - 1800) / 100);
        _axp20x_write_bytes(a, AXP192_LDO23OUT_VOL, 1, &rVal);
        return AXP20X_PASS;
    }
    return AXP20X_FAIL;
}

axp20x_millivolt axp20x_get_ldo3_voltage(struct axp20x * a) {
    uint8_t rVal;
    if (!a->_init)
        return AXP20X_NOT_INIT;

    if (a->_chip_id == AXP202_CHIP_ID) {
        _axp20x_read_bytes(a, AXP202_LDO3OUT_VOL, 1, &rVal);
        if (rVal & 0x80) {
            //! According to the hardware N_VBUSEN Pin selection
            return axp20x_get_vbus_voltage(a) * 1000;
        } else {
            return (axp20x_millivolt)((rVal & 0x7F) * 25 + 700);
        }
    } else if (a->_chip_id == AXP192_CHIP_ID ||a-> _chip_id == AXP173_CHIP_ID) {
        _axp20x_read_bytes(a, AXP192_LDO23OUT_VOL, 1, &rVal);
        rVal &= 0x0F;
        return (axp20x_millivolt)(rVal * 100 + 1800);
    }
    return 0;
}

int axp20x_set_ldo4_voltage(struct axp20x * a, axp20x_millivolt mv) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id != AXP173_CHIP_ID)
        return AXP20X_FAIL;

    if (mv < 700) {
        AXP20X_DEBUG("LDO4:Below settable voltage:700mV~3500mV");
        mv = 700;
    }
    if (mv > 3500) {
        AXP20X_DEBUG("LDO4:Above settable voltage:700mV~3500mV");
        mv = 3500;
    }
    uint8_t val = (mv - 700) / 25;
    _axp20x_write_bytes(a, AXP173_LDO4_VLOTAGE, 1, &val);
    return AXP20X_PASS;
}

axp20x_millivolt axp20x_get_ldo4_voltage(struct axp20x * a) {
    const uint16_t ldo4_table[] = {1250, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2500, 2700, 2800, 3000, 3100, 3200, 3300};
    if (!a->_init)
        return 0;
    uint8_t val = 0;
    switch (a->_chip_id) {
    case AXP173_CHIP_ID:
        _axp20x_read_bytes(a, AXP173_LDO4_VLOTAGE, 1, &val);
        return (axp20x_millivolt)(val * 25 + 700);
    case AXP202_CHIP_ID:
        _axp20x_read_bytes(a, AXP202_LDO24OUT_VOL, 1, &val);
        val &= 0xF;
        return (axp20x_millivolt)(ldo4_table[val]);
        break;
    case AXP192_CHIP_ID:
    default:
        break;
    }
    return (axp20x_millivolt)(0);
}


int axp20x_set_ldo4_voltage_table(struct axp20x * a, axp_ldo4_table_t param) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id == AXP202_CHIP_ID) {
        if (param >= AXP202_LDO4_MAX)
            return AXP20X_INVALID;
        uint8_t val;
        _axp20x_read_bytes(a, AXP202_LDO24OUT_VOL, 1, &val);
        val &= 0xF0;
        val |= param;
        _axp20x_write_bytes(a, AXP202_LDO24OUT_VOL, 1, &val);
        return AXP20X_PASS;
    }
    return AXP20X_FAIL;
}

#define AXP202_LDO3_MODE_LDO 0
#define AXP202_LDO3_MODE_DCIN 1

int axp20x_set_ldo3_mode(struct axp20x * a, uint8_t mode) {
    uint8_t val;
    if (a->_chip_id != AXP202_CHIP_ID)
        return AXP20X_FAIL;
    _axp20x_read_bytes(a, AXP202_LDO3OUT_VOL, 1, &val);
    if (mode) {
        val |= BIT_MASK(7);
    } else {
        val &= (~BIT_MASK(7));
    }
    _axp20x_write_bytes(a, AXP202_LDO3OUT_VOL, 1, &val);
    return AXP20X_PASS;
}

int axp20x_set_startup_time(struct axp20x * a, uint8_t param) {
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (param > sizeof(axp20x_startup_params) / sizeof(axp20x_startup_params[0]))
        return AXP20X_INVALID;
    _axp20x_read_bytes(a, AXP202_POK_SET, 1, &val);
    val &= (~0b11000000);
    val |= axp20x_startup_params[param];
    _axp20x_write_bytes(a, AXP202_POK_SET, 1, &val);
    return AXP20X_PASS;
}

int axp20x_set_long_press_time(struct axp20x * a, uint8_t param) {
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (param > sizeof(axp20x_long_press_params) / sizeof(axp20x_long_press_params[0]))
        return AXP20X_INVALID;
    _axp20x_read_bytes(a, AXP202_POK_SET, 1, &val);
    val &= (~0b00110000);
    val |= axp20x_long_press_params[param];
    _axp20x_write_bytes(a, AXP202_POK_SET, 1, &val);
    return AXP20X_PASS;
}

int axp20x_set_shutdown_time(struct axp20x * a, uint8_t param) {
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (param > sizeof(axp20x_shutdown_params) / sizeof(axp20x_shutdown_params[0]))
        return AXP20X_INVALID;
    _axp20x_read_bytes(a, AXP202_POK_SET, 1, &val);
    val &= (~0b00000011);
    val |= axp20x_shutdown_params[param];
    _axp20x_write_bytes(a, AXP202_POK_SET, 1, &val);
    return AXP20X_PASS;
}

int axp20x_set_time_out_shutdown(struct axp20x * a, bool en) {
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, AXP202_POK_SET, 1, &val);
    if (en)
        val |= (1 << 3);
    else
        val &= (~(1 << 3));
    _axp20x_write_bytes(a, AXP202_POK_SET, 1, &val);
    return AXP20X_PASS;
}

int axp20x_shutdown(struct axp20x * a) {
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, AXP202_OFF_CTL, 1, &val);
    val |= (1 << 7);
    _axp20x_write_bytes(a, AXP202_OFF_CTL, 1, &val);
    return AXP20X_PASS;
}

float axp20x_get_setting_charge_current(struct axp20x * a) {
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
    val &= 0b00000111;
    float cur = 300.0 + val * 100.0;
    AXP20X_DEBUG("Setting Charge current : %.2f mA\n", cur);
    return cur;
}

bool axp20x_is_charging_enable(struct axp20x * a)
{
    uint8_t val;
    if (!a->_init)
        return false;
    _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
    if (val & (1 << 7)) {
        AXP20X_DEBUG("Charging enable is enable\n");
        val = true;
    } else {
        AXP20X_DEBUG("Charging enable is disable\n");
        val = false;
    }
    return val;
}

int axp20x_enable_chargeing(struct axp20x * a, bool en) {
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
    val = en ? (val | _BV(7)) : val & (~_BV(7));
    _axp20x_write_bytes(a, AXP202_CHARGE1, 1, &val);
    return AXP20X_PASS;
}

int axp20x_set_charging_target_voltage(struct axp20x * a, axp_chargeing_vol_t param)
{
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (param > sizeof(axp20x_target_vol_params) / sizeof(axp20x_target_vol_params[0]))
        return AXP20X_INVALID;
    _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
    val &= ~(0b01100000);
    val |= axp20x_target_vol_params[param];
    _axp20x_write_bytes(a, AXP202_CHARGE1, 1, &val);
    return AXP20X_PASS;
}

int axp20x_get_batt_percentage(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id != AXP202_CHIP_ID)
        return AXP20X_NOT_SUPPORT;
    uint8_t val;
    if (!axp20x_is_battery_connected(a))
        return 0;
    _axp20x_read_bytes(a, AXP202_BATT_PERCENTAGE, 1, &val);
    if (!(val & BIT_MASK(7))) {
        return val & (~BIT_MASK(7));
    }
    return 0;
}

int axp20x_set_chg_ledmode(struct axp20x * a, axp_chgled_mode_t mode) {
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_OFF_CTL, 1, &val);
    val &= 0b11001111;
    val |= BIT_MASK(3);
    switch (mode) {
    case AXP20X_LED_OFF:
        _axp20x_write_bytes(a, AXP202_OFF_CTL, 1, &val);
        break;
    case AXP20X_LED_BLINK_1HZ:
        val |= 0b00010000;
        _axp20x_write_bytes(a, AXP202_OFF_CTL, 1, &val);
        break;
    case AXP20X_LED_BLINK_4HZ:
        val |= 0b00100000;
        _axp20x_write_bytes(a, AXP202_OFF_CTL, 1, &val);
        break;
    case AXP20X_LED_LOW_LEVEL:
        val |= 0b00110000;
        _axp20x_write_bytes(a, AXP202_OFF_CTL, 1, &val);
        break;
    default:
        return AXP20X_FAIL;
    }
    return AXP20X_PASS;
}

int axp20x_debug_charging(struct axp20x * a) {
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
    AXP20X_DEBUG("SRC REG:0x%x\n", val);
    if (val & (1 << 7)) {
        AXP20X_DEBUG("Charging enable is enable\n");
    } else {
        AXP20X_DEBUG("Charging enable is disable\n");
    }
    AXP20X_DEBUG("Charging target-voltage : 0x%x\n", ((val & 0b01100000) >> 5) & 0b11);
    if (val & (1 << 4)) {
        AXP20X_DEBUG("end when the charge current is lower than 15%% of the set value\n");
    } else {
        AXP20X_DEBUG(" end when the charge current is lower than 10%% of the set value\n");
    }
    val &= 0b00000111;
    AXP20X_DEBUG("Charge current : %.2f mA\n", 300.0 + val * 100.0);
    return AXP20X_PASS;
}

int axp20x_debug_status(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val, val1, val2;
    _axp20x_read_bytes(a, AXP202_STATUS, 1, &val);
    _axp20x_read_bytes(a, AXP202_MODE_CHGSTATUS, 1, &val1);
    _axp20x_read_bytes(a, AXP202_IPS_SET, 1, &val2);
    AXP20X_DEBUG("AXP202_STATUS:   AXP202_MODE_CHGSTATUS   AXP202_IPS_SET\n");
    AXP20X_DEBUG("0x%x\t\t\t 0x%x\t\t\t 0x%x\n", val, val1, val2);
    return AXP20X_PASS;
}

int axp20x_limiting_off(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    uint8_t val;
    _axp20x_read_bytes(a, AXP202_IPS_SET, 1, &val);
    if (a->_chip_id == AXP202_CHIP_ID) {
        val |= 0x03;
    } else {
        val &= ~(1 << 1);
    }
    _axp20x_write_bytes(a, AXP202_IPS_SET, 1, &val);
    return AXP20X_PASS;
}

// Only AXP129 chip and AXP173
int axp20x_set_dcdc1_voltage(struct axp20x * a, axp20x_millivolt mv) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id != AXP192_CHIP_ID &&a-> _chip_id != AXP173_CHIP_ID)
        return AXP20X_FAIL;
    if (mv < 700) {
        AXP20X_DEBUG("DCDC1:Below settable voltage:700mV~3500mV");
        mv = 700;
    }
    if (mv > 3500) {
        AXP20X_DEBUG("DCDC1:Above settable voltage:700mV~3500mV");
        mv = 3500;
    }
    uint8_t val = (mv - 700) / 25;
    //! axp192 and axp173 dc1 control register same
    _axp20x_write_bytes(a, AXP192_DC1_VLOTAGE, 1, &val);
    return AXP20X_PASS;
}

// Only AXP129 chip and AXP173
axp20x_millivolt axp20x_get_dcdc1_voltage(struct axp20x * a) {
    if (a->_chip_id != AXP192_CHIP_ID &&a-> _chip_id != AXP173_CHIP_ID)
        return AXP20X_FAIL;
    uint8_t val = 0;
    //! axp192 and axp173 dc1 control register same
    _axp20x_read_bytes(a, AXP192_DC1_VLOTAGE, 1, &val);
    return (axp20x_millivolt)(val * 25 + 700);
}


int axp20x_set_timer(struct axp20x * a, uint8_t minutes) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id == AXP202_CHIP_ID ||a-> _chip_id == AXP192_CHIP_ID) {
        if (minutes > 63) {
            return AXP20X_ARG_INVALID;
        }
        _axp20x_write_bytes(a, AXP202_TIMER_CTL, 1, &minutes);
        return AXP20X_PASS;
    }
    return AXP20X_NOT_SUPPORT;
}

int axp20x_off_timer(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id == AXP202_CHIP_ID ||a-> _chip_id == AXP192_CHIP_ID) {
        uint8_t minutes = 0x80;
        _axp20x_write_bytes(a, AXP202_TIMER_CTL, 1, &minutes);
        return AXP20X_PASS;
    }
    return AXP20X_NOT_SUPPORT;
}

int axp20x_clear_timer_status(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id == AXP202_CHIP_ID ||a-> _chip_id == AXP192_CHIP_ID) {
        uint8_t val;
        _axp20x_read_bytes(a, AXP202_TIMER_CTL, 1, &val);
        val |= 0x80;
        _axp20x_write_bytes(a, AXP202_TIMER_CTL, 1, &val);
        return AXP20X_PASS;
    }
    return AXP20X_NOT_SUPPORT;
}

bool axp20x_get_timer_status(struct axp20x * a) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id == AXP202_CHIP_ID ||a-> _chip_id == AXP192_CHIP_ID) {
        uint8_t val;
        _axp20x_read_bytes(a, AXP202_TIMER_CTL, 1, &val);
        return ( val & 0x80 ) >> 7;
    }
    return AXP20X_NOT_SUPPORT;
}


int _axp192_gpio_0_select(axp_gpio_mode_t mode) {
    switch (mode) {
    case AXP20X_IO_OUTPUT_LOW_MODE:
        return 0b101;
    case AXP20X_IO_INPUT_MODE:
        return 0b001;
    case AXP20X_IO_LDO_MODE:
        return 0b010;
    case AXP20X_IO_ADC_MODE:
        return 0b100;
    case AXP20X_IO_FLOATING_MODE:
        return 0b111;
    case AXP20X_IO_OPEN_DRAIN_OUTPUT_MODE:
        return 0;
    case AXP20X_IO_OUTPUT_HIGH_MODE:
    case AXP20X_IO_PWM_OUTPUT_MODE:
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int _axp192_gpio_1_select(axp_gpio_mode_t mode) {
    switch (mode) {
    case AXP20X_IO_OUTPUT_LOW_MODE:
        return 0b101;
    case AXP20X_IO_INPUT_MODE:
        return 0b001;
    case AXP20X_IO_ADC_MODE:
        return 0b100;
    case AXP20X_IO_FLOATING_MODE:
        return 0b111;
    case AXP20X_IO_OPEN_DRAIN_OUTPUT_MODE:
        return 0;
    case AXP20X_IO_PWM_OUTPUT_MODE:
        return 0b010;
    case AXP20X_IO_OUTPUT_HIGH_MODE:
    case AXP20X_IO_LDO_MODE:
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}


int _axp192_gpio_3_select(axp_gpio_mode_t mode) {
    switch (mode) {
    case AXP20X_IO_EXTERN_CHARGING_CTRL_MODE:
        return 0;
    case AXP20X_IO_OPEN_DRAIN_OUTPUT_MODE:
        return 1;
    case AXP20X_IO_INPUT_MODE:
        return 2;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int _axp192_gpio_4_select(axp_gpio_mode_t mode) {
    switch (mode) {
    case AXP20X_IO_EXTERN_CHARGING_CTRL_MODE:
        return 0;
    case AXP20X_IO_OPEN_DRAIN_OUTPUT_MODE:
        return 1;
    case AXP20X_IO_INPUT_MODE:
        return 2;
    case AXP20X_IO_ADC_MODE:
        return 3;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}


int _axp192_gpio_set(struct axp20x * a, axp_gpio_t gpio, axp_gpio_mode_t mode) {
    int rslt;
    uint8_t val;
    switch (gpio) {
    case AXP20X_GPIO_0: {
        rslt = _axp192_gpio_0_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP192_GPIO0_CTL, 1, &val);
        val &= 0xF8;
        val |= (uint8_t)rslt;
        _axp20x_write_bytes(a, AXP192_GPIO0_CTL, 1, &val);
        return AXP20X_PASS;
    }
    case AXP20X_GPIO_1: {
        rslt = _axp192_gpio_1_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP192_GPIO1_CTL, 1, &val);
        val &= 0xF8;
        val |= (uint8_t)rslt;
        _axp20x_write_bytes(a, AXP192_GPIO1_CTL, 1, &val);
        return AXP20X_PASS;
    }
    case AXP20X_GPIO_2: {
        rslt = _axp192_gpio_1_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP192_GPIO2_CTL, 1, &val);
        val &= 0xF8;
        val |= (uint8_t)rslt;
        _axp20x_write_bytes(a, AXP192_GPIO2_CTL, 1, &val);
        return AXP20X_PASS;
    }
    case AXP20X_GPIO_3: {
        rslt = _axp192_gpio_3_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP192_GPIO34_CTL, 1, &val);
        val &= 0xFC;
        val |= (uint8_t)rslt;
        _axp20x_write_bytes(a, AXP192_GPIO34_CTL, 1, &val);
        return AXP20X_PASS;
    }
    case AXP20X_GPIO_4: {
        rslt = _axp192_gpio_4_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP192_GPIO34_CTL, 1, &val);
        val &= 0xF3;
        val |= (uint8_t)rslt;
        _axp20x_write_bytes(a, AXP192_GPIO34_CTL, 1, &val);
        return AXP20X_PASS;
    }
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int _axp202_gpio_0_select(axp_gpio_mode_t mode)
{
    switch (mode) {
    case AXP20X_IO_OUTPUT_LOW_MODE:
        return 0;
    case AXP20X_IO_OUTPUT_HIGH_MODE:
        return 1;
    case AXP20X_IO_INPUT_MODE:
        return 2;
    case AXP20X_IO_LDO_MODE:
        return 3;
    case AXP20X_IO_ADC_MODE:
        return 4;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int _axp202_gpio_1_select(axp_gpio_mode_t mode)
{
    switch (mode) {
    case AXP20X_IO_OUTPUT_LOW_MODE:
        return 0;
    case AXP20X_IO_OUTPUT_HIGH_MODE:
        return 1;
    case AXP20X_IO_INPUT_MODE:
        return 2;
    case AXP20X_IO_ADC_MODE:
        return 4;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int _axp202_gpio_2_select(axp_gpio_mode_t mode)
{
    switch (mode) {
    case AXP20X_IO_OUTPUT_LOW_MODE:
        return 0;
    case AXP20X_IO_INPUT_MODE:
        return 2;
    case AXP20X_IO_FLOATING_MODE:
        return 1;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}


int _axp202_gpio_3_select(axp_gpio_mode_t mode)
{
    switch (mode) {
    case AXP20X_IO_INPUT_MODE:
        return 1;
    case AXP20X_IO_OPEN_DRAIN_OUTPUT_MODE:
        return 0;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int _axp202_gpio_set(struct axp20x * a, axp_gpio_t gpio, axp_gpio_mode_t mode)
{
    uint8_t val;
    int rslt;
    switch (gpio) {
    case AXP20X_GPIO_0: {
        rslt = _axp202_gpio_0_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP202_GPIO0_CTL, 1, &val);
        val &= 0b11111000;
        val |= (uint8_t)rslt;
        _axp20x_write_bytes(a, AXP202_GPIO0_CTL, 1, &val);
        return AXP20X_PASS;
    }
    case AXP20X_GPIO_1: {
        rslt = _axp202_gpio_1_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP202_GPIO1_CTL, 1, &val);
        val &= 0b11111000;
        val |= (uint8_t)rslt;
        _axp20x_write_bytes(a, AXP202_GPIO1_CTL, 1, &val);
        return AXP20X_PASS;
    }
    case AXP20X_GPIO_2: {
        rslt = _axp202_gpio_2_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP202_GPIO2_CTL, 1, &val);
        val &= 0b11111000;
        val |= (uint8_t)rslt;
        _axp20x_write_bytes(a, AXP202_GPIO2_CTL, 1, &val);
        return AXP20X_PASS;
    }
    case AXP20X_GPIO_3: {
        rslt = _axp202_gpio_3_select(mode);
        if (rslt < 0)return rslt;
        _axp20x_read_bytes(a, AXP202_GPIO3_CTL, 1, &val);
        val = rslt ? (val | BIT_MASK(2)) : (val & (~BIT_MASK(2)));
        _axp20x_write_bytes(a, AXP202_GPIO3_CTL, 1, &val);
        return AXP20X_PASS;
    }
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}


int axp20x_set_gpiomode(struct axp20x * a, axp_gpio_t gpio, axp_gpio_mode_t mode)
{
    if (!a->_init)
        return AXP20X_NOT_INIT;
    switch (a->_chip_id) {
    case AXP202_CHIP_ID:
        return _axp202_gpio_set(a, gpio, mode);
        break;
    case AXP192_CHIP_ID:
        return _axp192_gpio_set(a, gpio, mode);
        break;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}


int _axp20x_axp_irq_mask(axp_gpio_irq_t irq)
{
    switch (irq) {
    case AXP20X_IRQ_NONE:
        return 0;
    case AXP20X_IRQ_RISING:
        return BIT_MASK(7);
    case AXP20X_IRQ_FALLING:
        return BIT_MASK(6);
    case AXP20X_IRQ_DOUBLE_EDGE:
        return 0b1100000;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int _axp202_gpio_irq_set(struct axp20x * a, axp_gpio_t gpio, axp_gpio_irq_t irq)
{
    uint8_t reg;
    uint8_t val;
    int mask;
    mask = _axp20x_axp_irq_mask(irq);

    if (mask < 0)return mask;
    switch (gpio) {
    case AXP20X_GPIO_0:
        reg = AXP202_GPIO0_CTL;
        break;
    case AXP20X_GPIO_1:
        reg = AXP202_GPIO1_CTL;
        break;
    case AXP20X_GPIO_2:
        reg = AXP202_GPIO2_CTL;
        break;
    case AXP20X_GPIO_3:
        reg = AXP202_GPIO3_CTL;
        break;
    default:
        return AXP20X_NOT_SUPPORT;
    }
    _axp20x_read_bytes(a, reg, 1, &val);
    val = mask == 0 ? (val & 0b00111111) : (val | mask);
    _axp20x_write_bytes(a, reg, 1, &val);
    return AXP20X_PASS;
}


int axp20x_set_gpioirq(struct axp20x * a, axp_gpio_t gpio, axp_gpio_irq_t irq)
{
    if (!a->_init)
        return AXP20X_NOT_INIT;
    switch (a->_chip_id) {
    case AXP202_CHIP_ID:
        return _axp202_gpio_irq_set(a, gpio, irq);
    case AXP192_CHIP_ID:
    case AXP173_CHIP_ID:
        return AXP20X_NOT_SUPPORT;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int axp20x_set_ldo5_voltage(struct axp20x * a, axp_ldo5_table_t vol) {
    const uint8_t params[] = {
        0b11111000, //1.8V
        0b11111001, //2.5V
        0b11111010, //2.8V
        0b11111011, //3.0V
        0b11111100, //3.1V
        0b11111101, //3.3V
        0b11111110, //3.4V
        0b11111111, //3.5V
    };
    if (!a->_init)
        return AXP20X_NOT_INIT;
    if (a->_chip_id != AXP202_CHIP_ID)
        return AXP20X_NOT_SUPPORT;
    if (vol > sizeof(params) / sizeof(params[0]))
        return AXP20X_ARG_INVALID;
    uint8_t val = 0;
    _axp20x_read_bytes(a, AXP202_GPIO0_VOL, 1, &val);
    val &= 0b11111000;
    val |= params[vol];
    _axp20x_write_bytes(a, AXP202_GPIO0_VOL, 1, &val);
    return AXP20X_PASS;
}


int _axp202_gpio_write(struct axp20x * a, axp_gpio_t gpio, uint8_t val)
{
    uint8_t reg;
    uint8_t wVal = 0;
    switch (gpio) {
    case AXP20X_GPIO_0:
        reg = AXP202_GPIO0_CTL;
        break;
    case AXP20X_GPIO_1:
        reg = AXP202_GPIO1_CTL;
        break;
    case AXP20X_GPIO_2:
        reg = AXP202_GPIO2_CTL;
        if (val) {
            return AXP20X_NOT_SUPPORT;
        }
        break;
    case AXP20X_GPIO_3:
        if (val) {
            return AXP20X_NOT_SUPPORT;
        }
        _axp20x_read_bytes(a, AXP202_GPIO3_CTL, 1, &wVal);
        wVal &= 0b11111101;
        _axp20x_write_bytes(a, AXP202_GPIO3_CTL, 1, &wVal);
        return AXP20X_PASS;
    default:
        return AXP20X_NOT_SUPPORT;
    }
    _axp20x_read_bytes(a, reg, 1, &wVal);
    wVal = val ? (wVal | 1) : (wVal & 0b11111000);
    _axp20x_write_bytes(a, reg, 1, &wVal);
    return AXP20X_PASS;
}

int _axp202_gpio_read(struct axp20x * a, axp_gpio_t gpio) {
    uint8_t val;
    uint8_t reg = AXP202_GPIO012_SIGNAL;
    uint8_t offset;
    switch (gpio) {
    case AXP20X_GPIO_0:
        offset = 4;
        break;
    case AXP20X_GPIO_1:
        offset = 5;
        break;
    case AXP20X_GPIO_2:
        offset = 6;
        break;
    case AXP20X_GPIO_3:
        reg = AXP202_GPIO3_CTL;
        offset = 0;
        break;
    default:
        return AXP20X_NOT_SUPPORT;
    }
    _axp20x_read_bytes(a, reg, 1, &val);
    return val & BIT_MASK(offset) ? 1 : 0;
}

int axp20x_gpio_write(struct axp20x * a, axp_gpio_t gpio, uint8_t val) {
    if (!a->_init)
        return AXP20X_NOT_INIT;
    switch (a->_chip_id) {
    case AXP202_CHIP_ID:
        return _axp202_gpio_write(a, gpio, val);
    case AXP192_CHIP_ID:
    case AXP173_CHIP_ID:
        return AXP20X_NOT_SUPPORT;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int axp20x_gpio_read(struct axp20x * a, axp_gpio_t gpio)
{
    if (!a->_init)
        return AXP20X_NOT_INIT;
    switch (a->_chip_id) {
    case AXP202_CHIP_ID:
        return _axp202_gpio_read(a, gpio);
    case AXP192_CHIP_ID:
    case AXP173_CHIP_ID:
        return AXP20X_NOT_SUPPORT;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}



int axp20x_get_charge_current(struct axp20x * a) {
    int cur;
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    switch (a->_chip_id) {
    case AXP202_CHIP_ID:
        _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
        val &= 0x0F;
        cur =  val * 100 + 300;
        if (cur > 1800 || cur < 300)return 0;
        return cur;
    case AXP192_CHIP_ID:
    case AXP173_CHIP_ID:
        _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
        return val & 0x0F;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int axp20x_set_charge_current(struct axp20x * a, uint16_t mA) {
    uint8_t val;
    if (!a->_init)
        return AXP20X_NOT_INIT;
    switch (a->_chip_id) {
    case AXP202_CHIP_ID:
        _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
        val &= 0b11110000;
        mA -= 300;
        val |= (mA / 100);
        _axp20x_write_bytes(a, AXP202_CHARGE1, 1, &val);
        return AXP20X_PASS;
    case AXP192_CHIP_ID:
    case AXP173_CHIP_ID:
        _axp20x_read_bytes(a, AXP202_CHARGE1, 1, &val);
        val &= 0b11110000;
        if (mA > AXP1XX_CHARGE_CUR_1320MA)
            mA = AXP1XX_CHARGE_CUR_1320MA;
        val |= mA;
        _axp20x_write_bytes(a, AXP202_CHARGE1, 1, &val);
        return AXP20X_PASS;
    default:
        break;
    }
    return AXP20X_NOT_SUPPORT;
}

int axp20x_set_sleep(struct axp20x * a) {
    int ret;
    uint8_t val  = 0;
    ret = _axp20x_read_bytes(a, AXP202_VOFF_SET, 1, &val);
    if (ret != 0)return AXP20X_FAIL;
    val |= _BV(3);
    ret = _axp20x_write_bytes(a, AXP202_VOFF_SET, 1, &val);
    if (ret != 0)return AXP20X_FAIL;
    ret = _axp20x_read_bytes(a, AXP202_VOFF_SET, 1, &val);
    if (ret != 0)return AXP20X_FAIL;
    return (val & _BV(3)) ? AXP20X_PASS : AXP20X_FAIL;
}
