#include "ft6236.h"
#include "twali_i2c.h"


#define FT6236_REG_MODE         	(0x00)
#define FT6236_REG_GEST         	(0x01)
#define FT6236_REG_STATUS       	(0x02)
#define FT6236_REG_TOUCH1_XH    	(0x03)
#define FT6236_REG_TOUCH1_XL    	(0x04)
#define FT6236_REG_TOUCH1_YH    	(0x05)
#define FT6236_REG_TOUCH1_YL    	(0x06)
#define FT6236_REG_THRESHHOLD   	(0x80)
#define FT6236_REG_CONTROL      	(0x86)
#define FT6236_REG_MONITORTIME  	(0x87)
#define FT6236_REG_ACTIVEPERIOD  	(0x88)
#define FT6236_REG_MONITORPERIOD 	(0x89)

#define FT6236_REG_LIB_VERSIONH 	(0xa1)
#define FT6236_REG_LIB_VERSIONL 	(0xa2)
#define FT6236_REG_INT_STATUS   	(0xa4)
#define FT6236_REG_POWER_MODE   	(0xa5)
#define FT6236_REG_VENDOR_ID    	(0xa3)
#define FT6236_REG_VENDOR1_ID   	(0xa8)
#define FT6236_REG_ERROR_STATUS 	(0xa9)

/* Chip I2C registers. */

#define FT6236_REG_DEV_MODE				0x00	/* 0x00	[2:0]	Device Mode (better NOT write it!)	R/W*/
#define FT6236_REG_GEST_ID				0x01	/* 0x00	[7:0]	Gesture type	R                      */
#define FT6236_REG_TD_STATUS			0x02	/* 0x00	[3:0]	Number of touch points	R              */
#define FT6236_REG_P1_XH				0x03	/* 0xff	[7:0]	Touch 1 X position high byte | id.	R  */
#define FT6236_REG_P1_XL				0x04	/* 0xff	[7:0]	Touch 1 X position low byte.	R      */
#define FT6236_REG_P1_YH				0x05	/* 0xff	[7:0]	Touch 1 Y position high byte | id.	R  */
#define FT6236_REG_P1_YL				0x06	/* 0xff	[7:0]	Touch 1 Y position low byte.	R      */
#define FT6236_REG_P1_WEIGHT			0x07	/* 0xff	[7:0]	Touch 1 Weight	R                      */
#define FT6236_REG_P1_MISC				0x08	/* 0xff	[7:0]	Touch 1 Area	R                      */
#define FT6236_REG_P2_XH				0x09	/* 0xff	[7:0]	Touch 2 X position high byte | id.	R  */
#define FT6236_REG_P2_XL				0x0A	/* 0xff	[7:0]	Touch 2 X position low byte.	R      */
#define FT6236_REG_P2_YH				0x0B	/* 0xff	[7:0]	Touch 2 Y position high byte | id.	R  */
#define FT6236_REG_P2_YL				0x0C	/* 0xff	[7:0]	Touch 2 Y position low byte.	R      */
#define FT6236_REG_P2_WEIGHT			0x0D	/* 0xff	[7:0]	Touch 2 Weight	R                      */
#define FT6236_REG_P2_MISC				0x0E	/* 0xff	[7:0]	Touch 2 Area	R                      */
#define FT6236_REG_TH_GROUP				0x80	/* 0xff	[7:0]	Threshold for touch detection	R/W    */
#define FT6236_REG_TH_DIFF				0x85	/* 0xff	[7:0]	Filter function coefficient.	R/W    */
#define FT6236_REG_CTRL					0x86	/* 0x1	[7:0]	Monitor mode control	R/W        */
#define FT6236_REG_TIMEENTERMONITOR		0x87	/* 0xa	[7:0]	Time to switch mode from active to monitor	R/*/
#define FT6236_REG_PERIODACTIVE			0x88	/* 		[7:0]	Report rate in active mode	R/W                   */
#define FT6236_REG_PERIODMONITOR		0x89	/* 0x28	[7:0]	Report rate in monitor mode	R/W                   */
#define FT6236_REG_RADIAN_VALUE			0x91	/* 0xa	[7:0]	Minimum angle for rotation gestures	R/W       */
#define FT6236_REG_OFFSET_LEFT_RIGHT	0x92	/* 0x19	[7:0]	Maximum offset for left/right gestures	R/W       */
#define FT6236_REG_OFFSET_UP_DOWN		0x93	/* 0x19	[7:0]	Maximum offset for up/down gestures	R/W           */
#define FT6236_REG_DISTANCE_LEFT_RIGHT	0x94	/* 0x19	[7:0]	Minimum distance for left/right gestures	R/W   */
#define FT6236_REG_DISTANCE_UP_DOWN		0x95	/* 0x19	[7:0]	Minimum distance for up/down gestures	R/W       */
#define FT6236_REG_DISTANCE_ZOOM		0x96	/* 0x32	[7:0]	Maximum distance for zoom gestures	R/W           */
#define FT6236_REG_LIB_VER_H			0xA1	/* 		[7:0]	High byte of lib version info	R                 */
#define FT6236_REG_LIB_VER_L			0xA2	/* 		[7:0]	Low byte of lib version info.	R                 */
#define FT6236_REG_CIPHER				0xA3	/* 0x6	[7:0]	Chip Select	R                                 */
#define FT6236_REG_G_MODE				0xA4	/* 0x1	[7:0]	Interrupt mode,	R/W                           */
#define FT6236_REG_PWR_MODE				0xA5	/* 0x00	[7:0]	Current power mode of system	R/W               */
#define FT6236_REG_FIRMID				0xA6	/* 		[7:0]	Firmware Version	R                             */
#define FT6236_REG_FOCALTECH_ID			0xA8	/* 0x11	[7:0]	FocalTech's Panel ID	R                         */
#define FT6236_REG_RELEASE_CODE_ID		0xAF	/* 0x01	[7:0]	Release code version	R                         */
#define FT6236_REG_STATE				0xBC	/* 0x1	[7:0]	Current operating mode	R/W                   */

/* Constants */
#define FT6236_G_MODE_POLLING		0x00			
#define FT6236_G_MODE_TRIGGER		0x01			
#define FT6236_CTRL_KEEP_ACTIVE		0x00	/* Do not switch to monitor mode.                        */
#define FT6236_CTRL_AUTO_MONITOR	0x01	/* Automatically switch to monitor mode.                 */
#define FT6236_TD_STATUS_TOUCH_MASK	0x0f	/* Mask for number of touches. 							 */
#define FT6236_TD_STATUS_EVENT_MASK	0xC0	/* Mask for event .    							 		 */
#define FT6236_TOUCH_X_ID_MASK		0xC0	/* Mask for ID of the touch, which is the type,          */
#define FT6236_TOUCH_X_ID_SHIFT		0x06	/* Right shift for ID of the touch, which is the type,   */
#define FT6236_TOUCH_Y_ID_MASK		0xC0	/* Mask for ID of the touch, which is the real ID.       */
#define FT6236_TOUCH_Y_ID_SHIFT		0x04	/* Right shift for ID of the touch, which is the real ID.*/
#define FT6236_TOUCH_HIGH_MASK		0x0F	/* Mask for high byte of X or Y coord of touch           */


int ft6236_begin(struct ft6236 * f, uint16_t port, uint8_t addr) {
	f->_.bus = port;
	f->_.addr = addr;
	ft6236_set_register_u8(f, FT6236_REG_STATE, 1);
	ft6236_set_register_u8(f, FT6236_REG_DISTANCE_LEFT_RIGHT, 4);
	ft6236_set_register_u8(f, FT6236_REG_DISTANCE_UP_DOWN, 4);
	ft6236_set_register_u8(f, FT6236_REG_RADIAN_VALUE, 4);
	ft6236_set_register_u8(f, FT6236_REG_DISTANCE_ZOOM, 8);	
	// ft6236_set_register_u8(f, FT6236_REG_CTRL, 0);
	return 0;
}

uint8_t ft6236_get_register_u8(struct ft6236 *f, uint8_t reg) {
	uint8_t data;
	twali_i2c_get_bytes(f->_.bus, f->_.addr, reg, 1, &data);
	return data;	
}

uint8_t ft6236_set_register_u8(struct ft6236 *f, uint8_t reg, uint8_t value) {
	twali_i2c_set_bytes(f->_.bus, f->_.addr, reg, 1, &value);
	return value;	
}

int ft6236_read_bytes(struct ft6236 *f, uint8_t reg, uint8_t *data, uint8_t nbytes) {
	return twali_i2c_get_bytes(f->_.bus, f->_.addr, reg, nbytes, data);
}

int ft6236_write_bytes(struct ft6236 *f, uint8_t reg, uint8_t *data, uint8_t nbytes) {
	return twali_i2c_set_bytes(f->_.bus, f->_.addr, reg, nbytes, data);
}
 
uint8_t ft6236_get_control(struct ft6236 *f) {
	return ft6236_get_register_u8(f, FT6236_REG_CTRL);
}

uint8_t ft6236_get_mode(struct ft6236 *f) {
	return ft6236_get_register_u8(f, FT6236_REG_DEV_MODE);
}

enum ft6236_gesture ft6236_get_gesture(struct ft6236 *f) {
	return ft6236_get_register_u8(f, FT6236_REG_GEST_ID);
}


uint8_t ft6236_set_threshold(struct ft6236 *f, uint8_t threshold) {
	return ft6236_set_register_u8(f, FT6236_REG_TH_GROUP, threshold);
}

uint8_t ft6236_get_threshold(struct ft6236 *f, uint8_t threshold) {
	return ft6236_get_register_u8(f, FT6236_REG_TH_GROUP);
}
	

uint8_t ft6236_get_monitor_time(struct ft6236 *f) {
    return ft6236_get_register_u8(f, FT6236_REG_TIMEENTERMONITOR);
}

void ft6236_set_monitor_time(struct ft6236 *f, uint8_t sec) {
    ft6236_set_register_u8(f, FT6236_REG_TIMEENTERMONITOR, sec);
}

uint8_t ft6236_get_active_period(struct ft6236 *f) {
    return ft6236_get_register_u8(f, FT6236_REG_PERIODACTIVE);
}

void ft6236_set_active_period(struct ft6236 *f, uint8_t period) {
    ft6236_set_register_u8(f, FT6236_REG_PERIODACTIVE, period);
}

uint8_t ft6236_get_monitor_period(struct ft6236 *f) {
    return ft6236_get_register_u8(f,FT6236_REG_PERIODMONITOR);
}

void ft6236_set_monitor_period(struct ft6236 *f, uint8_t period) {
    ft6236_set_register_u8(f, FT6236_REG_PERIODMONITOR, period);
}

void ft6236_enable_auto_calibration(struct ft6236 *f) {
    ft6236_set_register_u8(f, FT6236_REG_MONITORTIME, 0x00);
}

void ft6236_disable_auto_calibration(struct ft6236 *f) {
    ft6236_set_register_u8(f, FT6236_REG_MONITORTIME, 0xfF);
}

void ft6236_get_library_version(struct ft6236 *f, uint16_t *version) {
    uint8_t buffer[2];
    ft6236_read_bytes(f, FT6236_REG_LIB_VER_H, buffer, 2);
    *version = (buffer[0] << 8) | buffer[1];
}

void ft6236_enable_int(struct ft6236 *f) {
    ft6236_set_register_u8(f, FT6236_REG_INT_STATUS, 1);
    
}

void ft6236_disable_int(struct ft6236 *f) {
    ft6236_set_register_u8(f, FT6236_REG_INT_STATUS, 0);
}

uint8_t ft6236_get_int_mode(struct ft6236 *f) {
    return ft6236_get_register_u8(f,FT6236_REG_INT_STATUS);
}

int ft6236_get_state(struct ft6236 *f,  struct ft6236_state * state) {
    uint8_t buffer[16];
    int i, res;
    res = ft6236_read_bytes(f, FT6236_REG_DEV_MODE, buffer, sizeof(buffer));
    if (res) {
		return res;
	} 
    if (!state) {
		state = &f->state;
	}
    state->gesture = buffer[1];
    state->count   = buffer[2] & FT6236_TD_STATUS_TOUCH_MASK;
    state->event   = buffer[2] >> 4;
    for (i = 0 ; i < 2 ; i++) {
		uint16_t hi_x, lo_x, hi_y, lo_y;
		hi_x  = buffer[(i * 6) + 3] & FT6236_TOUCH_HIGH_MASK;
		lo_x  = buffer[(i * 6) + 4];
		hi_y  = buffer[(i * 6) + 5] & FT6236_TOUCH_HIGH_MASK;
		lo_y  = buffer[(i * 6) + 6];
		
		state->touches[i].x =  hi_x << 8 | lo_x ; 
		state->touches[i].y =  hi_y << 8 | lo_y ;
		// XXX: Check this
		state->touches[i].id = (buffer[(i * 6) + 3] >> 6); 
		state->touches[i].weight = buffer[(i * 6) + 7];
		state->touches[i].area   = buffer[(i * 6) + 8];	
	}
	return 0;
}

uint8_t ft6236_get_touched(struct ft6236 *f) {
    return ft6236_get_register_u8(f,FT6236_REG_STATUS);
}

void ft6236_set_power_mode(struct ft6236 *f, uint8_t m) {
    ft6236_set_register_u8(f, FT6236_REG_POWER_MODE, m);
}

uint8_t ft6236_get_power_mode(struct ft6236 *f) {
    return ft6236_get_register_u8(f,FT6236_REG_POWER_MODE);
}

uint8_t ft6236_get_vendor_id(struct ft6236 *f) {
    return ft6236_get_register_u8(f,FT6236_REG_CIPHER);
}

uint8_t ft6236_get_firmware_id(struct ft6236 *f)
{
    return ft6236_get_register_u8(f,FT6236_REG_FIRMID);
}

uint8_t ft6236_get_error_code(struct ft6236 *f)
{
    return ft6236_get_register_u8(f,FT6236_REG_ERROR_STATUS);
}

