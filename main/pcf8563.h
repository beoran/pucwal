#ifndef PCF8563_H_INCLUDED
#define PCF8563_H_INCLUDED



#ifdef CPROTO
#else
#include <stdint.h>
#include <time.h>
#endif

#define PCF8563_SLAVE_ADDRESS 0x51

enum pcf8563_clkout_frequency {    
    PCF8563_CLKOUT_FREQUENCY_32768HZ = 0,
    PCF8563_CLKOUT_FREQUENCY_1024HZ,
    PCF8563_CLKOUT_FREQUENCY_32HZ,
    PCF8563_CLKOUT_FREQUENCY_1HZ,
    PCF8563_CLKOUT_FREQUENCY_DISABLED,
} ;


enum pcf8563_alarm_flags {   
	PCF8563_ALARM_FLAGS_MATCH_MINUTE  = 0x01, 
    PCF8563_ALARM_FLAGS_MATCH_HOUR    = 0x02, 
    PCF8563_ALARM_FLAGS_MATCH_DAY     = 0x04, 
    PCF8563_ALARM_FLAGS_MATCH_WEEKDAY = 0x08
};

enum pcf8563_timer_frequency {
    PCF8563_TIMER_FREQUENCY_4096HZ = 0,
    PCF8563_TIMER_FREQUENCY_64HZ,      
    PCF8563_TIMER_FREQUENCY_1HZ,       
    PCF8563_TIMER_FREQUENCY_1_60HZ,     
	PCF8563_TIMER_FREQUENCY_DISABLED,
};

struct pcf8563_state {
	struct tm time;
	struct tm alarm; 
};

struct pcf8563 {
	struct { 
		uint16_t bus;
		uint8_t addr;
		uint8_t init;
		uint16_t irq_status;
	}_;
	struct pcf8563_state state;
}; 


#include "pcf8563_proto.h"


#endif /* __PCF8563_H__ */
